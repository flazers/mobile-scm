//
//  Alerts.swift
//  FLAZE
//
//  Created by Sista, Siva Rama Krishna on 4/17/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import Foundation

class Alerts: NSObject {
    var item: [String:String] = [:]
    var organization: [String:String] = [:]
    var salesDate: [String:String] = [:]
    var bookedQuantity: [String:String] = [:]
    var shippedQuantity: [String:String] = [:]
    var forecast: [String:String] = [:]
    var variance: [String:String] = [:]
    var variancePercentage: [String:Int] = [:]
    var alertName: [String:String] = [:]
    var annualDemand: [String:String] = [:]
    var annualForecast: [String:Int] = [:]
    
    var headerItems:[String:String] = [:]
    var detailOnlyItems:[String:String] = [:]
    
    override init() {
        super.init()
    }
    
    required init?(json: [AnyObject]) {
        super.init()
        
        guard let dict = json[0] as? [String: AnyObject] else {
            return nil
        }
        
        if let id = dict["item"] as? [String:AnyObject] {
            guard let name = id["name"]  as? String else{
               return
            }
            
            guard let isHeader = id["isHeader"] as? Bool  else {
                return
            }
            
            item["name"] = name
            item["isHeader"] = "\(isHeader)"
            
            if isHeader {
                headerItems["item"] = name
            } else {
                detailOnlyItems["item"] = name
            }
        }
        
        if let id = dict["alertName"] as? [String:AnyObject] {
            guard let name = id["name"] as? String  else{
                return
            }
            
            guard let isHeader = id["isHeader"] as? Bool else {
                return
            }
            
            alertName["name"] = name
            alertName["isHeader"] = "\(isHeader)"
            
            if isHeader {
                headerItems["alertName"] = name
            }else {
                detailOnlyItems["alertName"] = name
            }
        }
        
        if let id = dict["annualDemand"] as? [String:AnyObject] {
            guard let name = id["name"] as? String  else{
                return
            }
            
            guard let isHeader = id["isHeader"] as? Bool else {
                return
            }
            
            annualDemand["name"] = name
            annualDemand["isHeader"] = "\(isHeader)"
            
            if isHeader {
                headerItems["annualDemand"] = name
            }else {
                detailOnlyItems["annualDemand"] = name
            }
        }
        
        if let id = dict["organization"]  {
            guard let name = id["name"] as? String  else{
                return
            }
            
            guard let isHeader = id["isHeader"] as? Bool else {
                return
            }
            
            organization["name"] = name
            organization["isHeader"] = "\(isHeader)"
            
            if isHeader {
                headerItems["organization"] = name
            }else {
                detailOnlyItems["organization"] = name
            }
        }
        if let id = dict["salesDate"]  {
            guard let name = id["name"] as? String  else{
                return
            }
            
            guard let isHeader = id["isHeader"] as? Bool else {
                return
            }
            
            salesDate["name"] = name
            salesDate["isHeader"] = "\(isHeader)"
            
            if isHeader {
                headerItems["salesDate"] = name
            }else {
                detailOnlyItems["salesDate"] = name
            }
        }
        
        if let id = dict["variancePercentage"]  {
            guard let name = id["name"]  else{
                return
            }
            
            guard let isHeader = id["isHeader"] as? Bool else {
                return
            }
            
            variancePercentage["name"] = name!.integerValue
            variancePercentage["isHeader"] = Int(isHeader)

            if isHeader {
                headerItems["variancePercentage"] = "\(name!.integerValue)"
            }else {
                detailOnlyItems["variancePercentage"] = "\(name!.integerValue)"
            }
        }
        
        if let id = dict["annualForecast"]  {
            guard let name = id["name"]  else{
                return
            }
            
            guard let isHeader = id["isHeader"] as? Bool else {
                return
            }
            
            annualForecast["name"] = name!.integerValue
            annualForecast["isHeader"] = Int(isHeader)
            
            if isHeader {
                headerItems["annualForecast"] = "\(name!.integerValue)"
            }else {
                detailOnlyItems["annualForecast"] = "\(name!.integerValue)"
            }
        }

        
        if let id = dict["bookedQuantity"]  {
            guard let name = id["name"]   else{
                return
            }
            
            guard let isHeader = id["isHeader"] as? Bool else {
                return
            }
            
            bookedQuantity["name"] = "\(name!.integerValue)"
            bookedQuantity["isHeader"] = "\(isHeader)"
            
            if isHeader {
                headerItems["bookedQuantity"] = "\(name!.integerValue)"
            }else {
                detailOnlyItems["bookedQuantity"] = "\(name!.integerValue)"
            }
        }
        
        if let id = dict["shippedQuantity"]  {
            guard let name = id["name"]  else{
                return
            }
            
            guard let isHeader = id["isHeader"] as? Bool else {
                return
            }
            
            shippedQuantity["name"] = "\(name!.integerValue)"
            shippedQuantity["isHeader"] = "\(isHeader)"
            
            if isHeader {
                headerItems["shippedQuantity"] = "\(name!.integerValue)"
            }else {
                detailOnlyItems["shippedQuantity"] = "\(name!.integerValue)"
            }
        }
        
        if let id = dict["forecast"]  {
            guard let name = id["name"]  else{
                return
            }
            
            guard let isHeader = id["isHeader"] as? Bool else {
                return
            }
            
            forecast["name"] = "\(name!.integerValue)"
            forecast["isHeader"] = "\(isHeader)"
            
            if isHeader {
                headerItems["forecast"] = "\(name!.integerValue)"
            }else {
                detailOnlyItems["forecast"] = "\(name!.integerValue)"
            }
        }
        
        if let id = dict["variance"]  {
            guard let name = id["name"]   else{
                return
            }
            
            guard let isHeader = id["isHeader"] as? Bool else {
                return
            }
            
            variance["name"] = "\(name!.integerValue)"
            variance["isHeader"] = "\(isHeader)"
            
            if isHeader {
                headerItems["variance"] = "\(name!.integerValue)"
            }else {
                detailOnlyItems["variance"] = "\(name!.integerValue)"
            }
        }
    }
    
    func getHeaders() -> [String:String] {
        return headerItems
    }
    
    func getDetailOnlyItems() -> [String:String] {
        return detailOnlyItems
    }
}
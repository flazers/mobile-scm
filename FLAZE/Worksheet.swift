//
//  Worksheet.swift
//  FLAZE
//
//  Created by Sista, Siva Rama Krishna on 3/29/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import Foundation

class Worksheet: NSObject {
    var worksheetID: String?
    var worksheetName: String?
    var userId: String?
    var userName: String?
    var threshold: String?
    
    override init() {
        super.init()
    }
    
    required init?(json: AnyObject) {
        super.init()
        
        guard let dict = json as? [String: AnyObject] else {
            return nil
        }
        
        if let id = dict["worksheetID"] as? String {
            worksheetID = id
        }
        
        if let name = dict["worksheetName"] as? String {
            worksheetName = name
        }
        
        if let uid = dict["userId"] as? String {
            userId = uid
        }
        
        if let uName = dict["userName"] as? String {
            userName = uName
        }
        
        if let uThreshold = dict["threshold"] as? String {
            threshold = uThreshold
        }
        
    }
}
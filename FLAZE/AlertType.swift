//
//  AlertType.swift
//  FLAZE
//
//  Created by Sista, Siva Rama Krishna on 3/29/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import Foundation

class AlertType: NSObject {
    var alertID: String?
    var alertName: String?
    
    override init() {
        super.init()
    }
    
    required init?(json: AnyObject) {
        super.init()
        
        guard let dict = json as? [String: AnyObject] else {
            return nil
        }
        
        if let id = dict["alertID"] as? String {
            alertID = id
        }
        
        if let name = dict["alertName"] as? String {
            alertName = name
        }
        
    }
}
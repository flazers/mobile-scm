//
//  ServicesRequest.swift
//  Connect
//
//  Created by swamy on 07/01/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import Foundation

typealias simpleCallback = (data: Dictionary<String, AnyObject>) -> Void
typealias dataCallback = (data : NSData?)-> Void

class ServicesRequest {
    
    static func loginImage( callback : (data : NSData?)-> Void){
        let urlString = CommonUiStyle.LOGIN_IMAGE
        let url = NSURL(string: urlString)
        let request : NSMutableURLRequest = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "GET"
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request){
            data, response, error in
                callback(data: data )
        }
        
        task.resume()
    }
    
    static func userLogin(username: String, password : String , callback : (data : Dictionary<String,AnyObject>)-> Void){
        var results :  [String : AnyObject] = [:]
        let appendString = "\(username.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()))&password=\(password)"
        let urlString = CommonUiStyle.USER_LOGIN + appendString
        let url = NSURL(string: urlString)
        let request : NSMutableURLRequest = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Accept")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request){
            data, response, error in
                if data?.length > 0 {
                    let json : JSON = JSON(data: data!)

                    for (key,subJson):(String, JSON) in json {
                        results[key] = subJson.rawValue
                    }
              }
            
              callback(data: results )
            }
        
        task.resume()
    }
    
    static func defaultSetup(callback : (data : Dictionary<String,AnyObject>)-> Void){
        var results :  [String : AnyObject] = [:]
        let urlString = CommonUiStyle.DEFAULT_SETUP
        let url = NSURL(string: urlString)
        let request : NSMutableURLRequest = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "GET"
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request){
            data, response, error in
            if data?.length > 0 {
                let json : JSON = JSON(data: data!)
                
                for (key,subJson):(String, JSON) in json {
                    results[key] = subJson.rawValue
                }
            }
            
            callback(data: results )
        }
        
        task.resume()
    }
    
    static func reportService(reportId: String, reportName : String , emailIds : String, callback : (data : Dictionary<String,AnyObject>)-> Void){
        var results :  [String : AnyObject] = [:]
        let userId = NSUserDefaults.standardUserDefaults().objectForKey("userID")
        let urlString = CommonUiStyle.SET_USER_REPORT
        let url = NSURL(string: urlString)
        let request : NSMutableURLRequest = NSMutableURLRequest(URL: url!)
        let jsonBody: [String: AnyObject] = [
            "userId": userId!,
            "reportId": reportId,
            "email": emailIds
        ]
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(jsonBody, options: NSJSONWritingOptions())
        let httpBody = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.HTTPBody = httpBody.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request){
            data, response, error in
            if data?.length > 0 {
                let json : JSON = JSON(data: data!)
                
                for (key,subJson):(String, JSON) in json {
                    results[key] = subJson.rawValue
                }
            }
            
            callback(data: results )
        }
        
        task.resume()
    }
    
    static func userSpecificNotifications(callback : (data : Dictionary<String,AnyObject>)-> Void){
        var results :  [String : AnyObject] = [:]
        let urlString = CommonUiStyle.GET_USER_NOTIFICATIONS
        let url = NSURL(string: urlString)
        let request : NSMutableURLRequest = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "GET"
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request){
            data, response, error in
            if data?.length > 0 {
                let json : JSON = JSON(data: data!)
                
                for (key,subJson):(String, JSON) in json {
                    results[key] = subJson.rawValue
                }
            }
            
            callback(data: results )
        }
        
        task.resume()
    }
    
    static func worksheetsByAlerts(alertId:String, callback : (data : Dictionary<String,AnyObject>)-> Void){
        var results :  [String : AnyObject] = [:]
        let userId = NSUserDefaults.standardUserDefaults().objectForKey("userID")
        let jsonBody: [String: AnyObject] = [
            "userId": userId!,
            "alertId": alertId
        ]
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(jsonBody, options: NSJSONWritingOptions())
        let httpBody = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
        let urlString = CommonUiStyle.GET_WORKSHEET_BY_ALERTS
        let url = NSURL(string: urlString)
        let request : NSMutableURLRequest = NSMutableURLRequest(URL: url!)
        
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.HTTPBody = httpBody.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request){
            data, response, error in
            if data?.length > 0 {
                let json : JSON = JSON(data: data!)
                
                for (key,subJson):(String, JSON) in json {
                    results[key] = subJson.rawValue
                }
            }
            
            callback(data: results )
        }
        
        task.resume()
    }
    
    static func updateNotifications(alertId: String, alertName : String ,worksheetId : String, worksheetName : String, threshold : String, callback : (data : Dictionary<String,AnyObject>)-> Void){
        var results :  [String : AnyObject] = [:]
        let userId = NSUserDefaults.standardUserDefaults().objectForKey("userID")
        let jsonBody: [String: AnyObject] = [
            "userId": userId!,
            "alertId": alertId,
            "worksheetId": worksheetId,
            "threshold": threshold
        ]
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(jsonBody, options: NSJSONWritingOptions())
        let httpBody = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
        let urlString = CommonUiStyle.UPDATE_NOTIFICATIONS
        let url = NSURL(string: urlString)
        let request : NSMutableURLRequest = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.HTTPBody = httpBody.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request){
            data, response, error in
            if data?.length > 0 {
                let json : JSON = JSON(data: data!)
                
                for (key,subJson):(String, JSON) in json {
                    results[key] = subJson.rawValue
                }
            }
            
            callback(data: results )
        }
        
        task.resume()
    }

    static func deviceToken(deviceToken: String, userID : String, status: String, callback : (data : Dictionary<String,AnyObject>)-> Void){
        var results :  [String : AnyObject] = [:]
        //let appendString = "\(deviceToken))"
        let appendString = "\(userID)&tokenId=\(deviceToken)&status=\(status)"
        
        let urlString = CommonUiStyle.DEVICE_TOKEN + appendString
        let url = NSURL(string: urlString)
        let request : NSMutableURLRequest = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Accept")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request){
            data, response, error in
            if data?.length > 0 {
                let json : JSON = JSON(data: data!)
                
                for (key,subJson):(String, JSON) in json {
                    results[key] = subJson.rawValue
                }
            }
            
            callback(data: results )
        }
        
        task.resume()
    }
    
    static func defaultAlerts(userID: String, date: String,  startRow: Int,  numberOfRows: Int, callback : (data : Dictionary<String,AnyObject>)-> Void){
        var results :  [String : AnyObject] = [:]
        let jsonBody: [String: AnyObject] = [
            "userId": userID,
            "date": date,
            "startRow": startRow,
            "numberOfRows": numberOfRows
        ]
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(jsonBody, options: NSJSONWritingOptions())
        let httpBody = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
        let urlString = CommonUiStyle.DEFAULT_ALERTS
        let url = NSURL(string: urlString)
        let request : NSMutableURLRequest = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.HTTPBody = httpBody.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)

        let task = NSURLSession.sharedSession().dataTaskWithRequest(request){
            data, response, error in
            if data?.length > 0 {
                let json : JSON = JSON(data: data!)
                
                for (key,subJson):(String, JSON) in json {
                    results[key] = subJson.rawValue
                }
            }
            
            callback(data: results )
        }
        
        task.resume()
    }
    
    static func filteredAlerts(userID: String, date: String,  startRow: Int,  numberOfRows: Int, alertID: String, callback : (data : Dictionary<String,AnyObject>)-> Void){
        var results :  [String : AnyObject] = [:]
        let jsonBody: [String: AnyObject] = [
            "userId": userID,
            "date": date,
            "startRow": startRow,
            "numberOfRows": numberOfRows,
            "alertName":alertID
        ]
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(jsonBody, options: NSJSONWritingOptions())
        let httpBody = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
        let urlString = CommonUiStyle.FILTER_ALERTS
        let url = NSURL(string: urlString)
        let request : NSMutableURLRequest = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.HTTPBody = httpBody.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request){
            data, response, error in
            if data?.length > 0 {
                let json : JSON = JSON(data: data!)
                
                for (key,subJson):(String, JSON) in json {
                    results[key] = subJson.rawValue
                }
            }
            
            callback(data: results )
        }
        
        task.resume()
    }
    
    static func searchAlerts(userID: String, date: String,  startRow: Int,  numberOfRows: Int,  item: String, organization: String, callback : (data : Dictionary<String,AnyObject>)-> Void){
        var results :  [String : AnyObject] = [:]
        let jsonBody: [String: AnyObject] = [
            "userId": userID,
            "date": date,
            "item": item,
            "organization": organization,
            "startRow": startRow,
            "numberOfRows": numberOfRows,
        ]
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(jsonBody, options: NSJSONWritingOptions())
        let httpBody = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
        let urlString = CommonUiStyle.SEARCH_ALERTS
        let url = NSURL(string: urlString)
        let request : NSMutableURLRequest = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.HTTPBody = httpBody.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request){
            data, response, error in
            if data?.length > 0 {
                let json : JSON = JSON(data: data!)
                
                for (key,subJson):(String, JSON) in json {
                    results[key] = subJson.rawValue
                }
            }
            
            callback(data: results )
        }
        
        task.resume()
    }
    
    static func supplyDemand(userID: String, callback : (data : Dictionary<String,AnyObject>)-> Void){
        var results :  [String : AnyObject] = [:]
        let jsonBody: [String: AnyObject] = [
            "userId": userID,
            "startRow": "1",
            "numberOfRows": "100"]
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(jsonBody, options: NSJSONWritingOptions())
        let httpBody = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
        let urlString = CommonUiStyle.SUPPLY_DEMAND
        let url = NSURL(string: urlString)
        let request : NSMutableURLRequest = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.HTTPBody = httpBody.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request){
            data, response, error in
            if data?.length > 0 {
                let json : JSON = JSON(data: data!)
                
                for (key,subJson):(String, JSON) in json {
                    results[key] = subJson.rawValue
                }
            }
            
            callback(data: results )
        }
        
        task.resume()
    }
    
    static func plannersData(callback : (data : Dictionary<String,AnyObject>)-> Void){
        let userId = NSUserDefaults.standardUserDefaults().objectForKey("userID")
        var results :  [String : AnyObject] = [:]
        let jsonBody: [String: AnyObject] = [
            "userId": userId! ]
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(jsonBody, options: NSJSONWritingOptions())
        let httpBody = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
        let urlString = CommonUiStyle.PLANNERS
        let url = NSURL(string: urlString)
        let request : NSMutableURLRequest = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.HTTPBody = httpBody.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request){
            data, response, error in
            if data?.length > 0 {
                let json : JSON = JSON(data: data!)
                
                for (key,subJson):(String, JSON) in json {
                    results[key] = subJson.rawValue
                }
            }
            
            callback(data: results )
        }
        
        task.resume()
    }
    
    static func dashboardData(callback : (data : Dictionary<String,AnyObject>)-> Void){
        let userId = NSUserDefaults.standardUserDefaults().objectForKey("userID")
        var results :  [String : AnyObject] = [:]
        let jsonBody: [String: AnyObject] = [
            "userId": userId! ]
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(jsonBody, options: NSJSONWritingOptions())
        let httpBody = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
        let urlString = CommonUiStyle.TOP_TEN_ITEMS
        let url = NSURL(string: urlString)
        let request : NSMutableURLRequest = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.HTTPBody = httpBody.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request){
            data, response, error in
            if data?.length > 0 {
                let json : JSON = JSON(data: data!)
                
                for (key,subJson):(String, JSON) in json {
                    results[key] = subJson.rawValue
                }
            }
            
            callback(data: results )
        }
        
        task.resume()
    }
    
    static func getMfgForecast(userID: String, callback : (data : Dictionary<String,AnyObject>)-> Void){
        var results :  [String : AnyObject] = [:]
        let jsonBody: [String: AnyObject] = [
            "userId": userID]
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(jsonBody, options: NSJSONWritingOptions())
        let httpBody = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
        let urlString = CommonUiStyle.MFGFORECAST
        let url = NSURL(string: urlString)
        let request : NSMutableURLRequest = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.HTTPBody = httpBody.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request){
            data, response, error in
            if data?.length > 0 {
                let json : JSON = JSON(data: data!)
                
                for (key,subJson):(String, JSON) in json {
                    results[key] = subJson.rawValue
                }
            }
            
            callback(data: results )
        }
        
        task.resume()
    }
    
    static func updateMfgForecast(userID: String, rowId: String,mfgApprovalFlag: String, mfgApproved: String, comments: String, callback : (data : Dictionary<String,AnyObject>)-> Void){
        var results :  [String : AnyObject] = [:]
        let jsonBody: [String: AnyObject] = [
            "userId": userID,
            "rowId": rowId,
            "mfgApprovalFlag": mfgApprovalFlag,
            "mfgApproved": mfgApproved,
            "comments": comments]
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(jsonBody, options: NSJSONWritingOptions())
        let httpBody = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
        let urlString = CommonUiStyle.UPDATEMFGFORECAST
        let url = NSURL(string: urlString)
        let request : NSMutableURLRequest = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.HTTPBody = httpBody.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request){
            data, response, error in
            if data?.length > 0 {
                let json : JSON = JSON(data: data!)
                
                for (key,subJson):(String, JSON) in json {
                    results[key] = subJson.rawValue
                }
            }
            
            callback(data: results )
        }
        
        task.resume()
    }
    
    static func sdTopThreshold(callback : (data : Dictionary<String,AnyObject>)-> Void){
        let userId = NSUserDefaults.standardUserDefaults().objectForKey("userID")
        var results :  [String : AnyObject] = [:]
        let jsonBody: [String: AnyObject] = [
            "userId": userId!,
            "startRow": 1,
            "numberOfRows": 50
        ]
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(jsonBody, options: NSJSONWritingOptions())
        let httpBody = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
        let urlString = CommonUiStyle.SPTOPTHRESHOLD
        let url = NSURL(string: urlString)
        let request : NSMutableURLRequest = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.HTTPBody = httpBody.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request){
            data, response, error in
            if data?.length > 0 {
                let json : JSON = JSON(data: data!)
                
                for (key,subJson):(String, JSON) in json {
                    results[key] = subJson.rawValue
                }
            }
            
            callback(data: results )
        }
        
        task.resume()
    }
    
    static func sdDetails(item: String, org: String, callback : (data : Dictionary<String,AnyObject>)-> Void){
        let userId = NSUserDefaults.standardUserDefaults().objectForKey("userID")
        var results :  [String : AnyObject] = [:]
        let jsonBody: [String: AnyObject] = [
            "userId": userId!,
            "item": item,
            "organization": org
        ]
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(jsonBody, options: NSJSONWritingOptions())
        let httpBody = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
        let urlString = CommonUiStyle.SPDETAILS
        let url = NSURL(string: urlString)
        let request : NSMutableURLRequest = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.HTTPBody = httpBody.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request){
            data, response, error in
            if data?.length > 0 {
                let json : JSON = JSON(data: data!)
                
                for (key,subJson):(String, JSON) in json {
                    results[key] = subJson.rawValue
                }
            }
            
            callback(data: results )
        }
        
        task.resume()
    }
}

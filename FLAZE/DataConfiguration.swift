//
//  DataConfiguration.swift
//  FLAZE
//
//  Created by Sista, Siva Rama Krishna on 3/18/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import UIKit

public class DataConfiguration:NSObject {
    public static let sharedInstance = DataConfiguration()
    public var primaryColor: String = ""
    public var secondaryColor: String = ""
    public var barTintColor: String = ""
    
    var alertTypes:[AlertType] = []
    var reportTypes:[ReportType] = []
    var workSheetList:[Worksheet] = []
    var timebucket:String = ""

 func initializeAllConfigurations() {
        let bundle = NSBundle.mainBundle()
    if let path = bundle.pathForResource("config", ofType: "strings") {
        let resultDictionary = NSMutableDictionary(contentsOfFile: path)
        primaryColor = resultDictionary!["PrimaryColor"] as! String
        secondaryColor = resultDictionary!["SecondaryColor"] as! String
        barTintColor = resultDictionary!["BarTintColor"] as! String
        }
    }
    
    func configureDefaultSetup(defaultData:[String:AnyObject] ) {
        if let setup = defaultData["setup"] {
            guard let alerts = setup["alerts"] as? [AnyObject] where !alerts.isEmpty else {
                return
            }
            
            guard let reports = setup["reports"] as? [AnyObject] where !reports.isEmpty else {
                return
            }
            
            guard let worksheet = setup["worksheet"] as? [AnyObject] where !worksheet.isEmpty else {
                return
            }
            
            guard let timebucket = setup["timebucket"] as? String where !worksheet.isEmpty else {
                return
            }
            
            self.setAlertsData(alerts)
            self.setReportsData(reports)
            self.setWorksheetData(worksheet)
            self.timebucket = timebucket
        }
    }
    
    func setAlertsData(alerts:[AnyObject]) {
        for alertList in alerts {
            if let alertType = AlertType(json: alertList) {
                alertTypes.append(alertType)
                
            }
        }
    }
    
    func setReportsData(reports:[AnyObject]) {
        for reportList in reports {
            if let reportType = ReportType(json: reportList) {
                reportTypes.append(reportType)
            }
        }
    }
    
    func setWorksheetData(workSheetData:[AnyObject]) {
        for workList in workSheetData {
            if let worksheet = Worksheet(json: workList) {
                workSheetList.append(worksheet)
            }
        }
    }
}




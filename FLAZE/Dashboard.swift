//
//  Dashboard.swift
//  FLAZE
//
//  Created by Sista, Siva Rama Krishna on 5/20/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import Foundation

class Dashboard:NSObject {
    var item: String?
    var variancePercent: Int?
    var priority:Int?
    
    override init() {
        super.init()
    }
    
    required init?(json: AnyObject) {
        super.init()
        
        guard let dict = json as? [String: AnyObject] else {
            return nil
        }
        
        if let id = dict["item"] as? String {
            item = id
        }
        
        if let name = dict["variancePercent"] as? Int {
            variancePercent = name
        }
        
        if let priority = dict["priority"] as? Int {
            self.priority = priority
        }
    }
}


//
//  HomeViewController.swift
//  SalesForce
//
//  Created by SRK-SK on 3/12/16.
//  Copyright © 2016 com.SRK. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, SMSegmentViewDelegate, PickerDelegateMethods, DoneActionDelegate {

    @IBOutlet weak var sidebarButton: UIBarButtonItem!
    @IBOutlet weak var alphaView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dayBeforeYesterdayTable: UITableView!
    @IBOutlet weak var yesterDayTable: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    var alphaViewFlag = false

    @IBOutlet weak var containerView: UIView!
    var reachability: Reachability?
    var loadingView : UIView?
    var alertsArray:[Alerts] = []
    var sortedalertsArray:[Alerts] = []
    var originalAlertsArray:[Alerts] = []
    var filtered : [Dictionary<String, String>] = []
    var searchstring = ""
    var itemsList = [String]()
    var segmentView: SMSegmentView!
    var margin: CGFloat = 10.0
    var customPicker : CustomPickerView?
    var filterPickerFlag : Bool = true
    let dataConfig = DataConfiguration.sharedInstance
    let userDefaults = NSUserDefaults.standardUserDefaults()
    var selectedIndex:Int = -1
    var isSortClicked:Bool = false
    var sortData:[String] = []
    var loadCount = 1
    var canReload = false
    var selectedTableView : UITableView!
    var customVC : CustomModalViewController?
    var maxNumberRows: Int = 50
    var startRow: Int = 1
    var searchString : String = ""
    var searchBy : String = ""
    var dataDisplayedBy: Int = 0
    var selectedAlertId: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title = "Alerts"
        self.customVC = CustomModalViewController(nibName: "CustomModalViewController", bundle: nil)
        self.customVC?.delegate = self
        //Send device token only once
        if (!userDefaults.boolForKey("isDeviceDataSent")) {
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.initializeNotificationServices()
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isDeviceDataSent")
        }
        
        sortData = ["Ascending", "Descending"]
        
        self.setNeedsStatusBarAppearanceUpdate()
        self.sidebarButton.target = self.revealViewController()
        self.sidebarButton.action = "revealToggle:"
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                
        if dataConfig.timebucket == "MONTH" {
            let dateComponents = NSDateComponents()
            dateComponents.month = -1
            
            let lastmonthDay = NSCalendar.currentCalendar().dateByAddingComponents(dateComponents, toDate: NSDate(), options: [])!
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "EEE, dd MMM yyy hh:mm:ss +zzzz"
            dateFormatter.dateFormat = "MMM"
            let lastmonth = dateFormatter.stringFromDate(lastmonthDay)
            
            let thismonthDay = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: 0, toDate: NSDate(), options: [])!
            dateFormatter.dateFormat = "EEE, dd MMM yyy hh:mm:ss +zzzz"
            dateFormatter.dateFormat = "MMM"
            let thismonth =  dateFormatter.stringFromDate(thismonthDay)
            dateComponents.month = 1
            
            let nextmonthDay = NSCalendar.currentCalendar().dateByAddingComponents(dateComponents, toDate: NSDate(), options: [])!
            dateFormatter.dateFormat = "EEE, dd MMM yyy hh:mm:ss +zzzz"
            dateFormatter.dateFormat = "MMM"
            
            let nextmonth =  dateFormatter.stringFromDate(nextmonthDay)
            itemsList = [lastmonth, thismonth, nextmonth]
        } else {
            let yesterday = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: -1, toDate: NSDate(), options: [])!
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "EEE, dd MMM yyy hh:mm:ss +zzzz"
            dateFormatter.dateFormat = "MM/dd"
            dateFormatter.dateFormat = "YYYY-MM-dd"
            let yesterdayDate = dateFormatter.stringFromDate(yesterday)
            
            let today = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: 0, toDate: NSDate(), options: [])!
            dateFormatter.dateFormat = "EEE, dd MMM yyy hh:mm:ss +zzzz"
            dateFormatter.dateFormat = "MM/dd"
            dateFormatter.dateFormat = "YYYY-MM-dd"
            let todayDate =  dateFormatter.stringFromDate(today)
            
            let dayBeforeYesterday = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: -2, toDate: NSDate(), options: [])!
            dateFormatter.dateFormat = "EEE, dd MMM yyy hh:mm:ss +zzzz"
            dateFormatter.dateFormat = "MM/dd"
            dateFormatter.dateFormat = "YYYY-MM-dd"
            let dayBeforeYesterdayDate =  dateFormatter.stringFromDate(dayBeforeYesterday)
            
            itemsList = [dayBeforeYesterdayDate,yesterdayDate, todayDate]
        }
     
        let segmentFrame = CGRect(x: self.margin, y: 72.0, width: self.view.frame.size.width - self.margin*2, height: 40.0)
        
        self.segmentView = SMSegmentView(frame: segmentFrame, separatorColour: UIColor(white: 0.95, alpha: 0.3), separatorWidth: 0.5, segmentProperties: [keySegmentTitleFont: UIFont.systemFontOfSize(12.0), keySegmentOnSelectionColour: CommonUiStyle.btnColor(), keySegmentOffSelectionColour: UIColor.lightGrayColor(), keyContentVerticalMargin: Float(10.0)])
        
        self.segmentView.delegate = self
        self.segmentView.backgroundColor = UIColor.clearColor()
        self.segmentView.layer.cornerRadius = 5.0
        self.segmentView.layer.borderColor = UIColor(white: 0.85, alpha: 1.0).CGColor
        self.segmentView.layer.borderWidth = 1.0
        
        let view = self.segmentView
        // Add segments
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let dayBeforeYesterdayString = dateFormatter.dateFromString(itemsList[0])
        let yesterdayString = dateFormatter.dateFromString(itemsList[1])
        let todayString = dateFormatter.dateFromString(itemsList[2])
        
        dateFormatter.dateFormat = "MM/dd"
        let dayBeforeYesterday =  dateFormatter.stringFromDate(dayBeforeYesterdayString!)
        let yesterday =  dateFormatter.stringFromDate(yesterdayString!)
        let today =  dateFormatter.stringFromDate(todayString!)
        
        view.addSegmentWithTitle(dayBeforeYesterday, onSelectionImage: UIImage(named: "clip_light"), offSelectionImage: UIImage(named: "clip"))
        view.addSegmentWithTitle(yesterday, onSelectionImage: UIImage(named: "bulb_light"), offSelectionImage: UIImage(named: "bulb"))
        view.addSegmentWithTitle(today, onSelectionImage: UIImage(named: "cloud_light"), offSelectionImage: UIImage(named: "cloud"))

        self.segmentView.selectSegmentAtIndex(2)
        self.view.addSubview(view)
        
        let tap = UITapGestureRecognizer(target: self, action: "dismissSearch")
        self.alphaView.addGestureRecognizer(tap)
    }
    
    func dismissSearch() -> Void {
        alphaViewFlag = true
        self.searchClicked([])
    }
    
    // SMSegment Delegate
    func segmentView(segmentView: SMBasicSegmentView, didSelectSegmentAtIndex index: Int) {
        /*
         Replace the following line to implement what you want the app to do after the segment gets tapped.
         */
        switch index {
        case 0:
            self.dayBeforeYesterdayTable.hidden = false
            self.tableView.hidden = true
            self.yesterDayTable.hidden = true
            startRow = 1
            alertsArray = []
            self.serviceCall("defaultAlerts",Date: itemsList[0],startRow: startRow, alertID: "")
            selectedIndex = 0
            dataDisplayedBy = 0
            self.selectedTableView = self.dayBeforeYesterdayTable
            break
        case 1:
            self.dayBeforeYesterdayTable.hidden = true
            self.tableView.hidden = true
            self.yesterDayTable.hidden = false
            startRow = 1
            self.serviceCall("defaultAlerts",Date: itemsList[1],startRow: startRow, alertID: "")
            selectedIndex = 1
            dataDisplayedBy = 0
            alertsArray = []
            self.selectedTableView = self.yesterDayTable
            break
        case 2:
            self.dayBeforeYesterdayTable.hidden = true
            self.tableView.hidden = false
            self.yesterDayTable.hidden = true
            startRow = 1
            dataDisplayedBy = 0
            alertsArray = []
            self.serviceCall("defaultAlerts",Date:itemsList[2],startRow: startRow, alertID: "")
            selectedIndex = 2
            self.selectedTableView = self.tableView
            break
        default:
            break
        }
    }
    
    
    func searchClick(searchString:String, SearchBy:String) {
        dataDisplayedBy = 2
        self.alphaView.hidden = true
        alphaViewFlag = false
        self.customVC?.willMoveToParentViewController(nil)
        let  shrinkTransform : CGAffineTransform = CGAffineTransformMakeScale(0.1, 0.1)
        UIView.animateWithDuration(0.3, animations: {
            self.customVC?.view.transform = shrinkTransform
            }, completion: {
                (success) in
                self.customVC?.view.removeFromSuperview()
                self.customVC?.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
                self.customVC?.removeFromParentViewController()
                self.containerView.hidden = true
                
        })
        
        if searchString.characters.count > 0 {
            self.searchString = searchString
        }
        
        if SearchBy.characters.count > 0 {
            self.searchBy = SearchBy
        }
        
        alertsArray = []
        self.serviceCall("searchAlerts",Date:itemsList[selectedIndex],startRow: 1, alertID: "")
    }
    
    @IBAction func searchClicked(sender: AnyObject) {

        if  self.customPicker?.window != nil {
            self.customPicker?.removeFromSuperview()
            filterPickerFlag = true
        }

        if alphaViewFlag == false {
            if(alertsArray.count == 0) {
                return
            }
            
            self.alphaView.hidden = false
            alphaViewFlag = true
            self.view.bringSubviewToFront(self.alphaView)
            self.view.bringSubviewToFront(self.containerView)

            /*Calling the addChildViewController: method also calls
             the child’s willMoveToParentViewController: method automatically */
            
            let headerData = alertsArray.first!.getHeaders()
            customVC!.itemArray = headerData
            self.addChildViewController(customVC!)
            customVC?.view.translatesAutoresizingMaskIntoConstraints = true
            customVC?.view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth , UIViewAutoresizing.FlexibleHeight]
            //2. Define the detail controller's view size
            customVC?.view.frame = self.containerView.bounds
            self.containerView.addSubview((customVC?.view)!)
            self.customVC?.view.alpha = 1.0
            self.alphaView.alpha = 0.3
            self.customVC?.didMoveToParentViewController(self)
            let transientTransform : CGAffineTransform = CGAffineTransformMakeScale(0.5, 0.5)
            self.customVC?.view.transform = transientTransform;
            
            let midX : CGFloat = CGRectGetMidX(self.view.bounds)
            let midY : CGFloat = CGRectGetMidY(self.view.bounds)
            self.alphaView.center = CGPointMake(midX, midY);
            
            UIView.animateWithDuration(0.3, animations: {
                self.customVC?.view.transform = CGAffineTransformMakeScale(1.0, 1.0);
                self.containerView.hidden = false
                }, completion: {
                    (success) in
            })
        }else {
            self.alphaView.hidden = true
            alphaViewFlag = false
            self.customVC?.willMoveToParentViewController(nil)
            let  shrinkTransform : CGAffineTransform = CGAffineTransformMakeScale(0.1, 0.1)
            UIView.animateWithDuration(0.3, animations: {
                self.customVC?.view.transform = shrinkTransform
                }, completion: {
                    (success) in
                    self.customVC?.view.removeFromSuperview()
                    self.customVC?.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
                    self.customVC?.removeFromParentViewController()
                    self.containerView.hidden = true
            })
        }
    }
    
    func doneOrCancelClick(value : String) {
        if value != "cancel" {
            dataDisplayedBy = 1
            let indexSelected = self.customPicker?.pickerView.selectedRowInComponent(0)
            if isSortClicked {
                let selectedName = sortData[indexSelected!]
                self.sortAlertsDataBy(selectedName)
            } else {
                let selectedName = dataConfig.alertTypes[indexSelected!]
                alertsArray = []
                selectedAlertId = selectedName.alertID!
                self.serviceCall("filteredAlerts",Date:itemsList[selectedIndex],startRow: 1, alertID:selectedAlertId)
            }
        }
       
        filterPickerFlag = true
        self.alphaView.hidden = true
        self.view.sendSubviewToBack(self.alphaView)
        let originalFrame : CGRect = CGRectMake( 0,
                                                 self.view.frame.size.height,
                                                 CGRectGetWidth(self.view!.frame),
                                                 266)
        UIView.animateWithDuration(0.4, animations: {
            self.customPicker?.frame = originalFrame
        })
        self.customPicker?.removeFromSuperview()
    }
    
    @IBAction func filterButtonClick(sender: AnyObject) {
        if filterPickerFlag == true {
            self.alphaView.hidden = false
            self.view.bringSubviewToFront(self.alphaView)
            filterPickerFlag = false
            isSortClicked = false
            self.customPicker = CustomPickerView.instanceFromNib()
            
            var alertNames:[String] = []            
            for alert in dataConfig.alertTypes {
                alertNames.append(alert.alertName!)
            }
            self.customPicker?.data = alertNames
            self.customPicker?.pickerDelegate = self
            // position view offscreen in the direction that is should appear from
            let originalFrame : CGRect = CGRectMake( 0,
                                                     self.view.frame.size.height - 266,
                                                     CGRectGetWidth(self.view!.frame),
                                                     266)
            self.customPicker!.frame = CGRectMake( CGRectGetMinX(self.customPicker!.frame),
                                                   self.view.frame.size.height,
                                                   CGRectGetWidth(self.customPicker!.frame),
                                                   CGRectGetHeight(self.customPicker!.frame))
            
            self.view.addSubview(self.customPicker!)
            
            UIView.animateWithDuration(0.4, animations: {
                self.customPicker?.frame = originalFrame
            })
        }
    }
    
    @IBAction func sortButtonClick(sender: AnyObject) {
        if filterPickerFlag == true {
            dataDisplayedBy = 3
            self.alphaView.hidden = false
            self.view.bringSubviewToFront(self.alphaView)
            filterPickerFlag = false
            isSortClicked = true
            self.customPicker = CustomPickerView.instanceFromNib()
            self.customPicker?.data = sortData
            self.customPicker?.pickerDelegate = self
            // position view offscreen in the direction that is should appear from
            let originalFrame : CGRect = CGRectMake( 0,
                                                     self.view.frame.size.height - 266,
                                                     CGRectGetWidth(self.view!.frame),
                                                     266)
            self.customPicker!.frame = CGRectMake( CGRectGetMinX(self.customPicker!.frame),
                                                   self.view.frame.size.height,
                                                   CGRectGetWidth(self.customPicker!.frame),
                                                   CGRectGetHeight(self.customPicker!.frame))
            
            self.view.addSubview(self.customPicker!)
            
            UIView.animateWithDuration(0.4, animations: {
                self.customPicker?.frame = originalFrame
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return alertsArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : UITableViewCell?
        if cell == nil {
            let toplevelObjects : NSArray = NSBundle.mainBundle().loadNibNamed("SpecificationsTableViewCell", owner: self, options: nil)
            cell = toplevelObjects[0] as? SpecificationsTableViewCell
        }
        
        let alert:Alerts
        if isSortClicked {
            alertsArray = sortedalertsArray
            isSortClicked = false
        }
        
        if alertsArray.count < indexPath.row {
            return cell!
        }
        
        alert = alertsArray[indexPath.row]
        let alertNameLabel = UILabel(frame: CGRectMake(8, 1, 320, 40))
        alertNameLabel.textAlignment = NSTextAlignment.Left
        alertNameLabel.font = UIFont.boldSystemFontOfSize(18)
        alertNameLabel.textColor = UIColor.blackColor()
        alertNameLabel.text = alert.alertName["name"]
        cell!.addSubview(alertNameLabel)
        
        let headerData = alert.getHeaders()
        let X = alertNameLabel.frame.origin.x
        var Y = alertNameLabel.frame.origin.y
        
        for (Key,Value) in headerData {
            if Key == "alertName" {
                continue
            }
            let keyLabel = UILabel(frame: CGRectMake(X + 10, Y + 25, 158, 40))
            keyLabel.textAlignment = NSTextAlignment.Left
            keyLabel.font = UIFont.boldSystemFontOfSize(15)
            keyLabel.textColor = UIColor.darkGrayColor()
            keyLabel.text = "\(Key.stringFromCamelCase()) :"
            
            let valueLabel = UILabel(frame: CGRectMake(X + 165, Y + 25, 320, 40))
            valueLabel.textAlignment = NSTextAlignment.Left
            valueLabel.font = UIFont.boldSystemFontOfSize(15)
            valueLabel.textColor = UIColor.darkGrayColor()
            valueLabel.text = Value
            
            Y = Y+20
            cell!.addSubview(keyLabel)
            cell!.addSubview(valueLabel)
        }
        cell?.selectionStyle = .None
        return cell!
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.whiteColor()
        if cell.respondsToSelector(Selector("setSeparatorInset:")){
            cell.separatorInset = UIEdgeInsetsZero
        }
        
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")){
            cell.preservesSuperviewLayoutMargins = false
        }
        
        if cell.respondsToSelector(Selector("setLayoutMargins:")){
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 126
    }
    
     
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        var offsetValue : CGFloat = 0.0
        if tableView.contentSize.height >= self.view.frame.size.height {
            offsetValue = tableView.contentSize.height
            
        }else {
            offsetValue = self.view.frame.size.height-120-116
        }
        
        if let bottomViewP = tableView.viewWithTag(111) {
            bottomViewP.removeFromSuperview()
        }
        
        if alertsArray.count > 0 {
            let bottomView = UIView(frame: CGRectMake(0,offsetValue,self.tableView.frame.size.width,400))
            bottomView.backgroundColor = UIColor.lightGrayColor()
            bottomView.tag = 111
            let activityView = UIActivityIndicatorView(activityIndicatorStyle: .White)
            activityView.hidesWhenStopped = false
            activityView.tag = 222
            activityView.center = CGPointMake(bottomView.center.x, 40)
            bottomView.addSubview(activityView)
            let loadingLabel = UILabel(frame: CGRectMake(0,activityView.center.y + 20,tableView.frame.size.width,21))
            loadingLabel.backgroundColor = UIColor.clearColor()
            loadingLabel.textColor = UIColor.whiteColor()
            loadingLabel.textAlignment = NSTextAlignment.Center
            loadingLabel.tag = 333
            bottomView.addSubview(loadingLabel)
            tableView.addSubview(bottomView)
            return UIView()
        }
        
        return nil
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let view = self.selectedTableView.viewWithTag(111)
        let activityview = self.selectedTableView.viewWithTag(222) as? UIActivityIndicatorView
        let loadingLabel = self.selectedTableView.viewWithTag(333) as? UILabel
        var offsetValue : CGFloat = 0.0
        if let bottomView = view
        {
            offsetValue = bottomView.frame.origin.y - self.selectedTableView.contentOffset.y
            if offsetValue <= self.view.frame.size.height - 240 {
                activityview?.startAnimating()
                loadingLabel?.text = "Loading"
                canReload = true
            }else {
                activityview?.stopAnimating()
                loadingLabel?.text = ""
            }
        }
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if canReload == true {
            loadCount += 1
            startRow = startRow + maxNumberRows
            
            if dataDisplayedBy == 0 {
                self.serviceCall("defaultAlerts",Date: itemsList[selectedIndex],startRow: startRow, alertID: "")
            } else if dataDisplayedBy == 1 {
                self.serviceCall("filteredAlerts",Date:itemsList[selectedIndex],startRow: startRow, alertID:selectedAlertId)
            } else if dataDisplayedBy == 2 {
                self.serviceCall("searchAlerts",Date:itemsList[selectedIndex],startRow: startRow, alertID: "")
            }
            canReload = false
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let detailView = self.storyboard?.instantiateViewControllerWithIdentifier("AlertsDetailViewController") as? AlertsDetailViewController
        detailView?.alerts =  alertsArray[indexPath.row] as Alerts

        self.navigationController?.pushViewController(detailView!, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField .resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        //http://stackoverflow.com/questions/24108447/filtering-dictionary-inside-an-array-in-swift
        searchstring = textField.text! + string
        filtered = [[:]]
        self.tableView.reloadData()
        return true
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return [.Portrait, .PortraitUpsideDown]
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    func serviceCall(serviceName: String, Date : String, startRow: Int, alertID: String) {
        do {
            let reachability = try Reachability.reachabilityForInternetConnection()
            self.reachability = reachability
        } catch ReachabilityError.FailedToCreateWithAddress(let address) {
            return
        } catch {}
        
        if let reachability = reachability {
            if reachability.isReachable() {
                loadingView = LoadingView.instanceFromNib()
                self.view.addSubview(loadingView!)
                let views = Dictionary(dictionaryLiteral:("loadingView",loadingView!))
                loadingView!.translatesAutoresizingMaskIntoConstraints = false
                
                //Horizontal constraints
                let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[loadingView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
                self.view.addConstraints(horizontalConstraints)
                
                //Vertical constraints
                let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[loadingView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
                self.view.addConstraints(verticalConstraints)
                
                if(serviceName == "defaultAlerts") {
                    self.defaultAlerts(serviceName,Date: Date, startRow: startRow)
                }
                
                if(serviceName == "filteredAlerts") {
                    self.filteredAlerts(serviceName,Date:Date, startRow: startRow, alertID: alertID)
                }
                
                if(serviceName == "searchAlerts") {
                    self.searchAlerts(serviceName,Date:Date, startRow: startRow, item: self.searchBy, org: self.searchString)
                }
                
            }else {
                let alert : UIAlertController = UIAlertController(title: "Message", message: "Please check internet connectivity", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
                    (action) in
                    alert.dismissViewControllerAnimated(true, completion: nil)
                }))
                self.loadingView?.removeFromSuperview()
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func defaultAlerts(serviceName: String, Date : String, startRow: Int){
        let defaultAlerts: simpleCallback = { (data: [String: AnyObject]) in
            dispatch_async(dispatch_get_main_queue(), {
                if data.count > 0 {
                    if let result = data["alerts"] as? [[String: AnyObject]] {
                        self.parseAlertsData(result, isDefault: true)
                    }
                }else {
                    self.errormessage()
                }
                
                self.loadingView?.removeFromSuperview()
            })
        }
        
        let userId = NSUserDefaults.standardUserDefaults().objectForKey("userID") as? String
        ServicesRequest.defaultAlerts(userId!, date: Date, startRow: startRow, numberOfRows: maxNumberRows, callback: defaultAlerts)
    }
    
    func filteredAlerts(serviceName: String,Date : String, startRow: Int, alertID: String){
        let filteredAlerts: simpleCallback = { (data: [String: AnyObject]) in
            dispatch_async(dispatch_get_main_queue(), {
                if data.count > 0 {
                    if let result = data["alerts"] as? [[String: AnyObject]] {
                        self.parseAlertsData(result, isDefault: false)
                    }
                }else {
                    self.errormessage()
                }
                
                self.loadingView?.removeFromSuperview()
            })
        }
        
        let userId = NSUserDefaults.standardUserDefaults().objectForKey("userID") as? String
        ServicesRequest.filteredAlerts(userId!, date: Date, startRow: startRow, numberOfRows: maxNumberRows, alertID:alertID, callback: filteredAlerts)
    }
    
    func searchAlerts(serviceName: String,Date : String, startRow: Int, item: String, org: String){
        let filteredAlerts: simpleCallback = { (data: [String: AnyObject]) in
            dispatch_async(dispatch_get_main_queue(), {
                if data.count > 0 {
                    if let result = data["alerts"] as? [[String: AnyObject]] {
                        self.parseAlertsData(result, isDefault: false)
                    }
                }else {
                    self.errormessage()
                }
                
                self.loadingView?.removeFromSuperview()
            })
        }
        
        let userId = NSUserDefaults.standardUserDefaults().objectForKey("userID") as? String
        ServicesRequest.searchAlerts(userId!, date: Date, startRow: startRow, numberOfRows: maxNumberRows, item: item, organization: org, callback: filteredAlerts)
    }
    
    func errormessage() {
        let alert : UIAlertController = UIAlertController(title: "Message", message: "Please try again", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
            (action) in
            alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func parseAlertsData(alertsData: [[String: AnyObject]], isDefault: Bool) {
        
        if alertsData.count == 0 && startRow <= 1{
            let alert : UIAlertController = UIAlertController(title: "Message", message: "No Alerts for this Date", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
                (action) in
                alert.dismissViewControllerAnimated(true, completion: nil)
            }))
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        originalAlertsArray = alertsArray
        
        for alerts in alertsData {
            if let alert = Alerts(json: [alerts]) {
                alertsArray.append(alert)
            }
        }
        
        tableView.reloadData()
        yesterDayTable.reloadData()
        dayBeforeYesterdayTable.reloadData()

    }
    
    func sortAlertsDataBy(sortString : String) {
        if sortString == "Ascending" {
            sortedalertsArray = alertsArray.sort({ $0.variancePercentage["name"] < $1.variancePercentage["name"] })
        }else if sortString == "Descending" {
            sortedalertsArray = alertsArray.sort({ $0.variancePercentage["name"] > $1.variancePercentage["name"] })
        }else {
            sortedalertsArray = originalAlertsArray
        }

        tableView.reloadData()
        yesterDayTable.reloadData()
        dayBeforeYesterdayTable.reloadData()
    }
}

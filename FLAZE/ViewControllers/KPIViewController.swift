//
//  KPIViewController.swift
//  FLAZE
//
//  Created by Sista, Siva Rama Krishna on 5/3/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import UIKit

class KPIViewController: UIViewController,UIWebViewDelegate {
    
    @IBOutlet weak var sidebarButton: UIBarButtonItem!
    
    // url to load
    let urlString : String = "http://mavlracorp.ddns.net:8080/Flaze/images/kpigcm.html"
    //let urlString = NSBundle.mainBundle().URLForResource("kpigc", withExtension:"html")
    
    // outlet - webview
    @IBOutlet var kpiWebView: UIWebView!
    
    // outlet - activity indicator i.e. spinner
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title = "KPI"
        
        self.setNeedsStatusBarAppearanceUpdate()
        
        self.sidebarButton.target = self.revealViewController()
        self.sidebarButton.action = "revealToggle:"
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        // set webview delegate
        self.kpiWebView.delegate = self
        
        // fit content within screen size.
        self.kpiWebView.scalesPageToFit = true
        
        // start spinner
        self.spinner.startAnimating()
        
        // load url content within webview
        //if let urlToBrowse = urlString {
//        if let urlToBrowse = NSURL(string: self.urlString) {
//            let urlRequest = NSURLRequest(URL: urlToBrowse)
//            self.kpiWebView.loadRequest(urlRequest)
//        }
        
       // let siteAddress = "https://domain:8443/path/to/page"
        let url = NSURL (string: urlString)
        let urlRequest = NSURLRequest(URL: url!)
        let urlConnection:NSURLConnection = NSURLConnection(request: urlRequest, delegate: self)!
        self.kpiWebView.loadRequest(urlRequest)

    }
    
    // MARK: - Web view delegate function
    
    func webViewDidFinishLoad(webView: UIWebView) {
        self.spinner.stopAnimating()
    }
    
    func connection(connection: NSURLConnection, canAuthenticateAgainstProtectionSpace protectionSpace: NSURLProtectionSpace) -> Bool{
        print("canAuthenticateAgainstProtectionSpace method Returning True")
        return true
    }
    
    
    func connection(connection: NSURLConnection, didReceiveAuthenticationChallenge challenge: NSURLAuthenticationChallenge){
        
        print("did autherntcationchallenge = \(challenge.protectionSpace.authenticationMethod)")
        
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust  {
            print("send credential Server Trust")
            let credential = NSURLCredential(forTrust: challenge.protectionSpace.serverTrust!)
            challenge.sender!.useCredential(credential, forAuthenticationChallenge: challenge)
            
        }else if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodHTTPBasic{
            print("send credential HTTP Basic")
            let defaultCredentials: NSURLCredential = NSURLCredential(user: "username", password: "password", persistence:NSURLCredentialPersistence.ForSession)
            challenge.sender!.useCredential(defaultCredentials, forAuthenticationChallenge: challenge)
            
        }else if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodNTLM{
            print("send credential NTLM")
            
        } else{
            challenge.sender!.performDefaultHandlingForAuthenticationChallenge!(challenge)
        }
    }

}

//
//  LoadingViewController.swift
//  SalesForce
//
//  Created by SRK-SK on 3/10/16.
//  Copyright © 2016 com.SRK. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController {
     let dataConfig = DataConfiguration.sharedInstance

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.setNeedsStatusBarAppearanceUpdate()
        self.performSelector("callafter:", withObject: "defaultSetup", afterDelay: 0.1)
    }

    func callafter(serviceName : String){
        let defaultSetup: simpleCallback = { (data: Dictionary<String, AnyObject>) in
            dispatch_async(dispatch_get_main_queue(), {
                if data.count > 0 {
                    self.dataConfig.configureDefaultSetup(data)
                    self.performSegueWithIdentifier("LoginView", sender: self)
                }else {
                    self.errormessage()
                }
            })
        }
        
        ServicesRequest.defaultSetup(defaultSetup)
    }
    
    func errormessage() {
        let alert : UIAlertController = UIAlertController(title: "Message", message: "Please try again", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
            (action) in
            alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

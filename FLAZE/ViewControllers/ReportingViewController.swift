//
//  ReportingViewController.swift
//  SalesForce
//
//  Created by SRK-SK on 3/13/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import UIKit

class ReportingViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, PickerDelegateMethods {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sidebarButton: UIBarButtonItem!
    @IBOutlet weak var alertTypeTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var addReportButton: UIButton!
    @IBOutlet weak var alphaView: UIView!
    var customPicker : CustomPickerView?
    var filterPickerFlag : Bool = true
    var selectedIndex = -1
    var reachability: Reachability?
    var loadingView : UIView?
    var value = 0
    let dropDown = DropDown()
    var reportData = [[String : String]]()
    let dataConfig = DataConfiguration.sharedInstance
    var reportIds:[String] = []
    var reportNames:[String] = []
    let userDefaults = NSUserDefaults.standardUserDefaults()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Report"
        self.sidebarButton.target = self.revealViewController()
        self.sidebarButton.action = "revealToggle:"
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.sendButton.layer.cornerRadius  = 6.0
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableViewAutomaticDimension;
        
        for report in dataConfig.reportTypes {
            reportNames.append(report.reportName!)
            reportIds.append(report.reportID!)
        }
        
        dropDown.dataSource = reportNames
        dropDown.selectionAction = { [unowned self] (index, item) in
            self.selectedIndex = index
            self.alertTypeTextField.text = item
        }
        
        dropDown.anchorView = alertTypeTextField
        dropDown.bottomOffset = CGPoint(x: 0, y:alertTypeTextField.bounds.height)

    }

    @IBAction func addReport(sender: AnyObject) {
        
        if self.alertTypeTextField.text?.characters.count == 0 {
            let alert=UIAlertController(title: "Message", message: "Please select report name to add", preferredStyle: UIAlertControllerStyle.Alert);
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil));
            self.presentViewController(alert, animated: true, completion: nil)
            return
 
        }
        
        let val = self.isValidEmail(self.emailField.text!)
        
        if val == false{
            // alert dialog
            let alert=UIAlertController(title: "Message", message: "Please enter valid email", preferredStyle: UIAlertControllerStyle.Alert);
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil));
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        
        reportData.append([self.alertTypeTextField.text! : self.emailField.text!])
        self.emailField.text = ""
        value += 1
        
        if value > 4
        {
            let alert=UIAlertController(title: "Message", message: "Reached Maximum number of email", preferredStyle: UIAlertControllerStyle.Alert);
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil));
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        
        self.tableView.hidden = false
        self.tableView.reloadData()
    }
    
    @IBAction func sendClicked(sender : UIButton)
    {
        guard let key = self.alertTypeTextField.text  where !key.isEmpty else {
            self.showAlertMessage("Please add reports to send")
            return
        }
        
        self.serviceCall("")
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return [.Portrait, .PortraitUpsideDown]
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    @IBAction func showOrDismiss(sender: AnyObject) {
        if dropDown.hidden {
            dropDown.show()
        } else {
            dropDown.hide()
        }
    }
    
    @IBAction func viewTapped() {
        view.endEditing(false)
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if textField.tag == 2 {
            return true
  
        }
        textField.resignFirstResponder()
        showPicker()
        return false
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reportData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : NotificationTableViewCell? = tableView.dequeueReusableCellWithIdentifier("cell") as? NotificationTableViewCell
        
        if cell == nil
        {
            let toplevelObjects : NSArray = NSBundle.mainBundle().loadNibNamed("NotificationTableViewCell", owner: self, options: nil)
            cell = toplevelObjects[0] as? NotificationTableViewCell
        }
        
        let dict = reportData[indexPath.row]
        let emailIDArray = Array(dict.values)
        cell?.emailLabel.text = emailIDArray[0]
        cell?.selectionStyle = .None
        return cell!
    }
    
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            reportData.removeAtIndex(indexPath.row)
            tableView.reloadData()
        }
    }
    
    func showPicker() -> Void {
        
        if filterPickerFlag == true {
            self.alphaView.hidden = false
            self.view.bringSubviewToFront(self.alphaView)
            filterPickerFlag = false
            self.customPicker = CustomPickerView.instanceFromNib()
           
            self.customPicker?.data = reportNames
            self.customPicker?.pickerDelegate = self
            // position view offscreen in the direction that is should appear from
            let originalFrame : CGRect = CGRectMake( 0,
                                                     self.view.frame.size.height - 266,
                                                     CGRectGetWidth(self.view!.frame),
                                                     266)
            self.customPicker!.frame = CGRectMake( CGRectGetMinX(self.customPicker!.frame),
                                                   self.view.frame.size.height,
                                                   CGRectGetWidth(self.customPicker!.frame),
                                                   CGRectGetHeight(self.customPicker!.frame))
            
            self.view.addSubview(self.customPicker!)
            
            UIView.animateWithDuration(0.4, animations: {
                self.customPicker?.frame = originalFrame
            })
        }
    }
    
    func doneOrCancelClick(value : String)
    {
        
        let indexSelected = self.customPicker?.pickerView.selectedRowInComponent(0)
        if value == "done" {
            self.alertTypeTextField.text = reportNames[indexSelected!]
            self.selectedIndex = indexSelected!
        }
        filterPickerFlag = true
        self.alphaView.hidden = true
        self.view.sendSubviewToBack(self.alphaView)
        let originalFrame : CGRect = CGRectMake( 0,
                                                 self.view.frame.size.height,
                                                 CGRectGetWidth(self.view!.frame),
                                                 266)
        UIView.animateWithDuration(0.4, animations: {
            self.customPicker?.frame = originalFrame
        })
        self.customPicker?.removeFromSuperview()
    }
    
    
    func serviceCall(serviceName : String)
    {
        do {
            let reachability = try Reachability.reachabilityForInternetConnection()
            self.reachability = reachability
        } catch ReachabilityError.FailedToCreateWithAddress(let address) {
            print("Unable to create\nReachability with address:\n\(address)")
            return
        } catch {}
        
        if let reachability = reachability {
            if reachability.isReachable() {
                loadingView = LoadingView.instanceFromNib()
                self.view.addSubview(loadingView!)
                let views = Dictionary(dictionaryLiteral:("loadingView",loadingView!))
                loadingView!.translatesAutoresizingMaskIntoConstraints = false
                //Horizontal constraints
                let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[loadingView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
                self.view.addConstraints(horizontalConstraints)
                
                //Vertical constraints
                let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[loadingView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
                self.view.addConstraints(verticalConstraints)
                self.performSelector("sendReportData:", withObject: "", afterDelay: 0.1)
            } else {
                let alert : UIAlertController = UIAlertController(title: "Message", message: "Please check internet connectivity", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
                    (action) in
                    alert.dismissViewControllerAnimated(true, completion: nil)
                }))
                
                self.loadingView?.removeFromSuperview()
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func sendReportData(serviceName : String){
        let sendReport: simpleCallback = { (data: Dictionary<String, AnyObject>) in
            dispatch_async(dispatch_get_main_queue(), {
                
                if data.count > 0
                {
                    if let result = data["update"] as? String
                    {
                        if result == "success" || result == "data already exists"
                        {
                            self.showAlertMessage("Report will be emailed shortly.")
                            self.reportData.removeAll()
                            self.tableView.reloadData()
                        }
                        else
                        {
                            self.showAlertMessage("Update Failed")
                        }
                    }
                    
                }else
                {
                    self.showAlertMessage("Please try again after some time")
                }
                
                self.loadingView?.removeFromSuperview()
            })
        }
        
        var emailIds : String = NSUserDefaults.standardUserDefaults().objectForKey("userEmail") as! String
        
        for i in 0 ..< reportData.count
        {
            let dict = reportData[i]
            let emailValues = Array(dict.values)
            emailIds =  emailIds + "," + emailValues[0]

        }
        
        ServicesRequest.reportService(self.reportIds[self.selectedIndex], reportName: self.reportNames[self.selectedIndex],emailIds: emailIds, callback: sendReport)
    }
    
    func showAlertMessage(message : String)
    {
        let alert : UIAlertController = UIAlertController(title: "Message", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
            (action) in
            alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
}

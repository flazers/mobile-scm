//
//  SidebarTableViewController.swift
//  SalesForce
//
//  Created by SRK-SK on 3/12/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import UIKit

class SidebarTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    var sections = [String]()
    var sectionData : Dictionary<String, [[String:String]]> =  Dictionary<String, [[String:String]]>()
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNeedsStatusBarAppearanceUpdate()
        
        userName.text = self.userDefaults.objectForKey("userName") as? String
        userEmail.text = self.userDefaults.objectForKey("userEmail") as? String
        
        
            sectionData = ["0" :  [["MenuID" :"DASHBOARD", "Name":"Dashboard", "Image":"alerts"],
                ["MenuID" :"ALERTS", "Name":"Alerts", "Image":"alerts"],
                ["MenuID" :"REPORTS", "Name":"Reports", "Image":"reports"],
                ["MenuID" :"KPI", "Name":"KPIs", "Image":"kpi"],
                //["MenuID" :"SOP", "Name":"S&OP", "Image":"supplydemand"],
                ["MenuID" :"SIMULATION", "Name":"Simulation", "Image":"reports"],
                ["MenuID" :"NOTIFSETT", "Name":"Notification Settings", "Image":"notificationSetings"]],
                "1" : [["MenuID" :"SOP", "Name":"Supply & Demand", "Image":"supplydemand"]],
                "2" : [["MenuID" :"MFGFORECAST", "Name":"Manfacturer Forecast", "Image":"alerts"]],
                "3" : [["MenuID" :"DAILYORDERS", "Name":"Daily Orders", "Image":"dailyReport"],
                    ["MenuID" :"ORDERHISTORY", "Name":"Order History", "Image":"orderHistory"],
                    ["MenuID" :"SALESTOTALS", "Name":"Sales Totals", "Image":"salesTotal"],
                    ["MenuID" :"RTPP", "Name":"Real Time Product Pricing", "Image":"realTimeProduct"],
                    ["MenuID" :"LOGOUT", "Name":"Logout", "Image":"logout"]]]
            
            sections = ["  Integrated Business Planning", "  Sales & Ops Planning", "  Business Partner", "  Sales and Pricing"]
                

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    // MARK: - Table view data source
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sectionData.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionRowsCount = sectionData["\(section)"]
        return (sectionRowsCount?.count)!
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : MenuTableViewCell?
        let resuseIndentifier = "Cell"
        
        cell  = tableView.dequeueReusableCellWithIdentifier(resuseIndentifier) as? MenuTableViewCell
        if cell == nil
        {
            let toplevelObjects : NSArray = NSBundle.mainBundle().loadNibNamed("MenuTableViewCell", owner: self, options: nil)
            cell = toplevelObjects[0] as? MenuTableViewCell
        }
        
        let sectionRowsCount = sectionData["\(indexPath.section)"]
        cell?.productImgView?.image = UIImage(named:sectionRowsCount![indexPath.row]["Image"]!)
        cell?.descriptionLabel?.text = sectionRowsCount![indexPath.row]["Name"]
        cell!.selectionStyle = .None
        return cell!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.whiteColor()
        if cell.respondsToSelector(Selector("setSeparatorInset:")){
            cell.separatorInset = UIEdgeInsetsZero
        }
        
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")){
            cell.preservesSuperviewLayoutMargins = false
        }
        
        if cell.respondsToSelector(Selector("setLayoutMargins:")){
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 3
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRectMake(0, 10, self.view.frame.size.width, 30))
        view.backgroundColor = UIColor.whiteColor()
        let sectionTitleLabel = UILabel(frame: view.frame)
        sectionTitleLabel.backgroundColor = UIColor.clearColor()
        sectionTitleLabel.textColor = UIColor.darkGrayColor()
        sectionTitleLabel.font = UIFont.boldSystemFontOfSize(16)
        sectionTitleLabel.text = sections[section]
        view.addSubview(sectionTitleLabel)
        return view
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let sectionRowsCount = sectionData["\(indexPath.section)"]
        
        switch sectionRowsCount![indexPath.row]["MenuID"]! {
            
        case "DASHBOARD":
            if let dashboardNavController = self.storyboard?.instantiateViewControllerWithIdentifier("dashboard") as? UINavigationController
            {
                self.revealViewController().pushFrontViewController(dashboardNavController, animated: true)
            }
            
        case "ALERTS":
            if let homeScreenNavController = self.storyboard?.instantiateViewControllerWithIdentifier("HomeScreen") as? UINavigationController
            {
                self.revealViewController().pushFrontViewController(homeScreenNavController, animated: true)
            }
            
        case "REPORTS":
            if let reportNavController = self.storyboard?.instantiateViewControllerWithIdentifier("ReportScreen") as? UINavigationController
            {
                self.revealViewController().pushFrontViewController(reportNavController, animated: true)
            }
            
        case "KPI":
            if let kpiNavController = self.storyboard?.instantiateViewControllerWithIdentifier("KPI") as? UINavigationController
            {
                self.revealViewController().pushFrontViewController(kpiNavController, animated: true)
            }
        case "SOP":
            if let sopNavController = self.storyboard?.instantiateViewControllerWithIdentifier("SOP") as? UINavigationController
            {
                self.revealViewController().pushFrontViewController(sopNavController, animated: true)
            }
            
        case "NOTIFSETT":
            if let notificationNavController = self.storyboard?.instantiateViewControllerWithIdentifier("NotificationScreen") as? UINavigationController
            {
                self.revealViewController().pushFrontViewController(notificationNavController, animated: true)
            }
            
        case "LOGOUT":
            
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "loginStatus")
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "RememberMe")
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isDeviceDataSent")
            
            self.performSelector("logOut", withObject:nil, afterDelay: 0.1)
            
            let loginStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let loginViewController : ViewController = (loginStoryboard.instantiateViewControllerWithIdentifier("LoginScreen") as? ViewController)!
            let navigationCOntroller  = UINavigationController(rootViewController: loginViewController)
            self.presentedViewController?.dismissViewControllerAnimated(false, completion: nil)
            self.presentViewController(navigationCOntroller, animated: false, completion: nil)
            
        case "MFGFORECAST":
            if let mfgNavController = self.storyboard?.instantiateViewControllerWithIdentifier("BP") as? UINavigationController
            {
                self.revealViewController().pushFrontViewController(mfgNavController, animated: true)
            }
            
        default:
            break
            
        }
        
        
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return [.Portrait, .PortraitUpsideDown]
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    func logOut(){
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let userID = userDefaults.objectForKey("userID")  as! String
        if let deviceToken = userDefaults.objectForKey("DeviceToken") {
            let deviceTokenCall: simpleCallback = { (data: Dictionary<String, AnyObject>) in
                dispatch_async(dispatch_get_main_queue(), {
                    if data.count > 0 {
                        if let result = data["userStatus"] as? String {
                            if result == "success" {
                            }else{
                                //self.errormessage()
                            }
                        }
                    }else {
                        
                    }
                })
            }
            
            ServicesRequest.deviceToken(deviceToken as! String,userID: userID,status: "out", callback: deviceTokenCall)
        }
    }
}

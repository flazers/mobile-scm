//
//  CustomModalViewController.swift
//  ModelView
//
//  Created by swamy on 29/04/16.
//  Copyright © 2016 swamy. All rights reserved.
//

import UIKit

protocol DoneActionDelegate {
    func searchClick(searchString:String, SearchBy:String)
}

class CustomModalViewController: UIViewController {
    @IBOutlet weak var searchButton: UIButton!

    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var searchKeyTxtView: UITextField!
    @IBOutlet weak var searchTxtView: UITextField!
    let dropDown = DropDown()
    var selectedIndex = -1
    var itemArray:[String:String] = [:]
    
    //var search:DoneActionDelegate?
    var delegate:DoneActionDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchButton.layer.cornerRadius  = 6.0

        dropDown.dataSource = ["Item", "Organization", "Sales Date"]
        dropDown.selectionAction = { [unowned self] (index, item) in
            self.selectedIndex = index
            self.searchKeyTxtView.text = item
        }
        self.innerView.layer.cornerRadius = 12.0
        dropDown.anchorView = searchKeyTxtView
        dropDown.bottomOffset = CGPoint(x: 0, y:searchKeyTxtView.bounds.height)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func searchClicked(sender: AnyObject) {
        delegate.searchClick(searchTxtView.text!, SearchBy:searchKeyTxtView.text!)
    }
    
    @IBAction func showOrDismiss(sender: AnyObject) {
        if dropDown.hidden {
            dropDown.show()
        } else {
            dropDown.hide()
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
   
}

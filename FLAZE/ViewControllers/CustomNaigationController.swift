//
//  CustomNaigationController.swift
//  SalesForce
//
//  Created by SRK-SK on 3/9/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import UIKit

class CustomNaigationController: UINavigationController {

    override func shouldAutorotate() -> Bool {
        return (self.visibleViewController?.shouldAutorotate())!
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return (self.topViewController?.supportedInterfaceOrientations())!
    }

}

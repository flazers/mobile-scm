//
//  SOPMonthCollectionViewController.swift
//  FLAZE
//
//  Created by Sista, Siva Rama Krishna on 5/13/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import UIKit

class SOPMonthCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    let dateCellIdentifier = "DateCellIdentifier"
    let contentCellIdentifier = "ContentCellIdentifier"
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var orgLabel: UILabel!
    @IBOutlet weak var itemLabel: UILabel!
     @IBOutlet weak var itemCostLabel: UILabel!
    
    var reachability: Reachability?
    var loadingView : UIView?
    var monthlyData  = [String:AnyObject]()
    var org:String?
    var item:String?
    var headers:[String]?
    var sopTitle:[String]?
    var respData  = [[String:AnyObject]]()
    var months:[String]?
    var monthData = [[String]]()
    var organization:String?
    var items:String?
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Item Details - Monthly"
        months = ["Month:", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        sopTitle = ["Month:", "Total Supply:", "Total Demand:", "Excess:", "Ending Balance(Proj):"]
        
        org = organization
        item = items 
        
        orgLabel.text = "Organization: " + org!
        itemLabel.text =  "Items: " + item!
        
        
        self.setNeedsStatusBarAppearanceUpdate()
        
        self.collectionView .registerNib(UINib(nibName: "DateCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: dateCellIdentifier)
        self.collectionView .registerNib(UINib(nibName: "ContentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: contentCellIdentifier)
        
         self.serviceCall("supplyDemand")
                
    }
    
    func serviceCall(serviceName: String) {
        do {
            let reachability = try Reachability.reachabilityForInternetConnection()
            self.reachability = reachability
        } catch ReachabilityError.FailedToCreateWithAddress(let address) {
            return
        } catch {}
        
        if let reachability = reachability {
            if reachability.isReachable() {
                loadingView = LoadingView.instanceFromNib()
                self.view.addSubview(loadingView!)
                let views = Dictionary(dictionaryLiteral:("loadingView",loadingView!))
                loadingView!.translatesAutoresizingMaskIntoConstraints = false
                
                //Horizontal constraints
                let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[loadingView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
                self.view.addConstraints(horizontalConstraints)
                
                //Vertical constraints
                let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[loadingView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
                self.view.addConstraints(verticalConstraints)
                
                if(serviceName == "supplyDemand") {
                    self.supplyDemadData()
                }
                
                
            }else {
                let alert : UIAlertController = UIAlertController(title: "Message", message: "Please check internet connectivity", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
                    (action) in
                    alert.dismissViewControllerAnimated(true, completion: nil)
                }))
                self.loadingView?.removeFromSuperview()
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func supplyDemadData() {
        let supplyDemand: simpleCallback = { (data: [String: AnyObject]) in
            dispatch_async(dispatch_get_main_queue(), {
                    if data.count > 0 {
                        let sd = data["supplyDemand"]!
                        self.respData = (sd as? [[String:AnyObject]])!
                        self.parseData();
                    }else {
                        self.errormessage()
                    }
                
                self.loadingView?.removeFromSuperview()
            })
        }
        
        ServicesRequest.sdDetails(item!,  org: org!, callback: supplyDemand)
    }
    
    func parseData() {
        for id in self.respData {
            var dataArray:[String] = []
            
            guard let supply = id["totalSupply"]!["name"]  else {
                return
            }
            
            guard let demand = id["totalDemand"]!["name"]  else {
                return
            }
            
            guard let cost = id["itemCost"]!["name"]  else {
                return
            }
            
            guard let excess = id["excess"]!["name"]  else {
                return
            }
            
            guard let projectedEndingBalance = id["projectedEndingBalance"]!["name"]  else {
                return
            }
            
            self.itemCostLabel.text = "Item Cost: " + String(cost as! Int)
            
            dataArray.append("")
            dataArray.append(String(supply as! Int))
            dataArray.append(String(demand as! Int))
            //dataArray.append(String(cost as! Int))
            dataArray.append(String(excess as! Int))
            dataArray.append(String(projectedEndingBalance as! Int))
            
            monthData.append(dataArray)
        }
        
        self.collectionView.reloadData()
    }
    
    func errormessage() {
        let alert : UIAlertController = UIAlertController(title: "Message", message: "Please try again", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
            (action) in
            alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    // MARK - UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 5
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return months!.count
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let dateCell : DateCollectionViewCell = collectionView .dequeueReusableCellWithReuseIdentifier(dateCellIdentifier, forIndexPath: indexPath) as! DateCollectionViewCell
                dateCell.dateLabel.font = UIFont.boldSystemFontOfSize(15)
                dateCell.dateLabel.textColor = UIColor.darkGrayColor()
                dateCell.dateLabel.text = "Month:"
                
                return dateCell
            } else {
                let contentCell : ContentCollectionViewCell = collectionView .dequeueReusableCellWithReuseIdentifier(contentCellIdentifier, forIndexPath: indexPath) as! ContentCollectionViewCell
                contentCell.contentLabel.font = UIFont.boldSystemFontOfSize(15)
                contentCell.contentLabel.textColor = UIColor.darkGrayColor()
                contentCell.contentLabel.text = months![indexPath.row]
                
                
                return contentCell
            }
        } else {
            if indexPath.row == 0 {
                let dateCell : DateCollectionViewCell = collectionView .dequeueReusableCellWithReuseIdentifier(dateCellIdentifier, forIndexPath: indexPath) as! DateCollectionViewCell
                dateCell.dateLabel.font = UIFont.boldSystemFontOfSize(15)
                dateCell.dateLabel.textColor = UIColor.darkGrayColor()
                dateCell.dateLabel.text = sopTitle![indexPath.section]

                
                return dateCell
            } else {
                let contentCell : ContentCollectionViewCell = collectionView .dequeueReusableCellWithReuseIdentifier(contentCellIdentifier, forIndexPath: indexPath) as! ContentCollectionViewCell
                contentCell.contentLabel.font = UIFont.systemFontOfSize(15)
                contentCell.contentLabel.textColor = UIColor.darkGrayColor()
                if monthData.count > 0 {
                    contentCell.contentLabel.text = monthData[indexPath.row - 1][indexPath.section]
                }
                
                contentCell.userInteractionEnabled = false
                
                return contentCell
            }
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath){
        
//        let backItem = UIBarButtonItem()
//        backItem.title = "Monthly"
//        navigationItem.backBarButtonItem = backItem
//        
//        let dayView = self.storyboard?.instantiateViewControllerWithIdentifier("SOPDay") as? SOPDayCollectionViewController
//        
//        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
//        self.navigationController?.pushViewController(dayView!, animated: true)
        
    }
 

}

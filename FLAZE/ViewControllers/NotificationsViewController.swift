//
//  NotificationsViewController.swift
//  SalesForce
//
//  Created by SRK-SK on 3/12/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController, UITextFieldDelegate, PickerDelegateMethods {

    @IBOutlet weak var alphaView: UIView!

    var reachability: Reachability?
    var loadingView : UIView?
    @IBOutlet weak var alertTypeTextField: UITextField!
    @IBOutlet weak var sidebarButton: UIBarButtonItem!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var workSheetTextField: UITextField!
    @IBOutlet weak var thresholdTextField: UITextField!
    
    var customPicker : CustomPickerView?
    var filterPickerFlag : Bool = true
    
    let dataConfig = DataConfiguration.sharedInstance
    let dropDownAlertType = DropDown()
    let dropDownWorkSheet = DropDown()
    let dropDownThreshold = DropDown()
    var selectedAlertName = -1
    var selectedWorkSheet = -1
    var selectedThreshold:String?
    var alertNames:[String] = []
    var alertIds:[String] = []
    var workSheetNames:[String] = []
    var workSheetIds:[String] = []
    var workSheetThreshold:[String] = []
    var threshold:[String] = []
    var selectedTextField : UITextField = UITextField()
    var notificationData = [Dictionary<String, AnyObject>]()
    var workSheetData = [Dictionary<String, AnyObject>]()
    var selectedTag = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNeedsStatusBarAppearanceUpdate()

        // Do any additional setup after loading the view.
        self.title = "Notifications"
        self.sidebarButton.target = self.revealViewController()
        self.sidebarButton.action = "revealToggle:"
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.actionButton.layer.cornerRadius  = 6.0

        for alert in dataConfig.alertTypes {
            alertNames.append(alert.alertName!)
            alertIds.append(alert.alertID!)
        }
        
        dropDownAlertType.dataSource = alertNames
        dropDownWorkSheet.dataSource = workSheetNames
        for i in 0 ..< 51 {
            threshold.append("\(i)")
        }
        
        dropDownThreshold.dataSource = threshold
        dropDownAlertType.selectionAction = { [unowned self] (index, item) in
            self.selectedAlertName = index
            self.alertTypeTextField.text = item
            self.serviceCall("getWorksheets")
        }
        
        dropDownWorkSheet.selectionAction = { [unowned self] (index, item) in
            self.selectedWorkSheet = index
            self.workSheetTextField.text = item
        }
        
        dropDownThreshold.selectionAction = { [unowned self] (index, item) in
            self.selectedThreshold = String(index)
            self.thresholdTextField.text = item
        }
        
        dropDownAlertType.anchorView = alertTypeTextField
        dropDownAlertType.bottomOffset = CGPoint(x: 0, y:alertTypeTextField.bounds.height)
        
        dropDownWorkSheet.anchorView = workSheetTextField
        dropDownWorkSheet.bottomOffset = CGPoint(x: 0, y:workSheetTextField.bounds.height)
        
        dropDownThreshold.anchorView = thresholdTextField
        dropDownThreshold.bottomOffset = CGPoint(x: 0, y:thresholdTextField.bounds.height)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        selectedTag = textField.tag
        showPicker(textField.tag)
        return false
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func showPicker(tag : Int) -> Void {
        
        if filterPickerFlag == true
        {
            filterPickerFlag = false
            self.customPicker = CustomPickerView.instanceFromNib()
            var zeroState: Bool = false
            
            switch tag {
            case 1:
                self.customPicker?.data = alertNames
                break
            case 2:
                if workSheetNames.count > 0 {
                    self.customPicker?.data = workSheetNames
                } else {
                    self.customPicker?.hidden = true
                    filterPickerFlag = true
                    zeroState = true
                }
                break
            case 3:
                self.customPicker?.data = threshold
                break
            default:
                break
                
            }
            
            if zeroState == false {
                self.alphaView.hidden = false
                self.view.bringSubviewToFront(self.alphaView)
                self.customPicker?.pickerDelegate = self
                // position view offscreen in the direction that is should appear from
                let originalFrame : CGRect = CGRectMake( 0,
                    self.view.frame.size.height - 266,
                    CGRectGetWidth(self.view!.frame),
                    266)
                self.customPicker!.frame = CGRectMake( CGRectGetMinX(self.customPicker!.frame),
                    self.view.frame.size.height,
                    CGRectGetWidth(self.customPicker!.frame),
                    CGRectGetHeight(self.customPicker!.frame))
                
                self.view.addSubview(self.customPicker!)
                
                UIView.animateWithDuration(0.4, animations: {
                    self.customPicker?.frame = originalFrame
                })
            }
        }
    }
    
    func doneOrCancelClick(value : String) {
           let indexSelected = self.customPicker?.pickerView.selectedRowInComponent(0)
            filterPickerFlag = true
            self.alphaView.hidden = true
            self.view.sendSubviewToBack(self.alphaView)
            if value == "done" {
                switch self.selectedTag {
                case 1:
                    if alertNames.count > 0 {
                        alertTypeTextField.text = self.alertNames[indexSelected!]
                        self.selectedAlertName = indexSelected!
                        self.workSheetTextField.text = ""
                        self.thresholdTextField.text = ""
                        self.serviceCall("getWorksheets")
                    }
                    break
                case 2:
                    if self.workSheetNames.count > 0 {
                        self.workSheetTextField.text = self.workSheetNames[indexSelected!]
                        self.thresholdTextField.text = self.workSheetThreshold[indexSelected!]
                        self.selectedWorkSheet = indexSelected!
                        self.selectedThreshold = self.workSheetThreshold[indexSelected!]
                    }
                    break
                case 3:
                    self.thresholdTextField.text = self.threshold[indexSelected!]
                    self.selectedThreshold = self.threshold[indexSelected!]
                    break
                default:
                    break
                }
            }
            let originalFrame : CGRect = CGRectMake( 0,
                                                     self.view.frame.size.height,
                                                     CGRectGetWidth(self.view!.frame),
                                                     266)
            UIView.animateWithDuration(0.4, animations: {
                self.customPicker?.frame = originalFrame
            })
            self.customPicker?.removeFromSuperview()
        }
        
    @IBAction func showOrDismiss(sender: AnyObject) {
        let button = sender as! UIButton
        switch button.tag {
        case 1:
            if dropDownAlertType.hidden {
                dropDownAlertType.show()
            } else {
                dropDownAlertType.hide()
            }
            
            break
        case 2:
            if dropDownWorkSheet.hidden {
                dropDownWorkSheet.show()
            } else {
                dropDownWorkSheet.hide()
            }
            
            break
        case 3:
            if dropDownThreshold.hidden {
                dropDownThreshold.show()
            } else {
                dropDownThreshold.hide()
            }
            
            break
        default:
            break
        }
    }
    
    @IBAction func viewTapped() {
        view.endEditing(false)
    }
    
    @IBAction func saveClicked(sender: AnyObject) {
        if workSheetTextField.text?.characters.count == 0 || thresholdTextField.text?.characters.count == 0 || alertTypeTextField.text?.characters.count == 0
        {
            let alert : UIAlertController = UIAlertController(title: "Message", message: "Please select the values", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
                (action) in
                alert.dismissViewControllerAnimated(true, completion: nil)
            }))
            self.presentViewController(alert, animated: true, completion: nil)

            return
        }
        
        self.serviceCall("sendData")
    }
    
    func serviceCall(serviceName : String)
    {
        do {
            let reachability = try Reachability.reachabilityForInternetConnection()
            self.reachability = reachability
        } catch ReachabilityError.FailedToCreateWithAddress(let address) {
            print("Unable to create\nReachability with address:\n\(address)")
            return
        } catch {}
        
        if let reachability = reachability {
            if reachability.isReachable() {
                loadingView = LoadingView.instanceFromNib()
                
                self.view.addSubview(loadingView!)
                
                let views = Dictionary(dictionaryLiteral:("loadingView",loadingView!))
                loadingView!.translatesAutoresizingMaskIntoConstraints = false
                //Horizontal constraints
                let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[loadingView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
                self.view.addConstraints(horizontalConstraints)
                
                //Vertical constraints
                let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[loadingView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
                self.view.addConstraints(verticalConstraints)
                self.performSelector("notificationServiceCall:", withObject: serviceName, afterDelay: 0.1)
            }else {
                let alert : UIAlertController = UIAlertController(title: "Message", message: "Please check internet connectivity", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
                    (action) in
                    alert.dismissViewControllerAnimated(true, completion: nil)
                }))
                
                self.loadingView?.removeFromSuperview()
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func notificationServiceCall(serviceName : String) {
        let getNotifications: simpleCallback = { (data: Dictionary<String, AnyObject>) in
            dispatch_async(dispatch_get_main_queue(), {
                
                if data.count > 0 {
                    self.notificationData = (data["alerts"] as? [Dictionary<String, AnyObject>])!
                    if self.notificationData.count > 0 {
                        self.alertTypeTextField.text = self.notificationData[0]["alertName"] as? String
                        let alertId = self.notificationData[0]["id"] as! String
                        self.selectedAlertName = self.alertIds.indexOf(alertId)!
                    }
                    
                }else {
                     self.showAlertMessage("Please try again after some time")
                }
                
                self.loadingView?.removeFromSuperview()
            })
        }
        
        let getWorksheetsByAlerts: simpleCallback = { (data: Dictionary<String, AnyObject>) in
            dispatch_async(dispatch_get_main_queue(), {
                if data.count > 0 {
                    self.thresholdTextField.text = ""
                    self.workSheetTextField.text = ""
                    self.workSheetData = (data["worksheets"] as? [Dictionary<String, AnyObject>])!
                    self.storeWorksheet()
                }else {
                    self.showAlertMessage("Please try again after some time")
                }
                
                self.loadingView?.removeFromSuperview()
            })
        }
       
        let updateNotifications: simpleCallback = { (data: Dictionary<String, AnyObject>) in
            dispatch_async(dispatch_get_main_queue(), {
                print(data)
                if data.count > 0 {
                    if let result = data["update"] as? String {
                        if result == "success" {
                            self.showAlertMessage("Updated Successfully")
                            self.thresholdTextField.text = ""
                            self.alertTypeTextField.text = ""
                            self.workSheetTextField.text = ""
                            self.workSheetNames = []
                            self.workSheetThreshold = []
                            
                        }else {
                            self.showAlertMessage("Update Failed")
                        }
                    }
                }else {
                    self.showAlertMessage("Please try again after some time")
                }
                
                self.loadingView?.removeFromSuperview()
            })
        }
        
        if serviceName == "sendData" {
            ServicesRequest.updateNotifications(self.alertIds[self.selectedAlertName], alertName: self.alertNames[self.selectedAlertName], worksheetId: workSheetIds[self.selectedWorkSheet], worksheetName: workSheetNames[self.selectedWorkSheet], threshold: self.selectedThreshold!, callback: updateNotifications)

        }else if serviceName == "getAlerts" {
            ServicesRequest.userSpecificNotifications(getNotifications)
        } else if serviceName == "getWorksheets" {
            ServicesRequest.worksheetsByAlerts(self.alertIds[self.selectedAlertName], callback:getWorksheetsByAlerts)
        }
    }
    
    func showAlertMessage(message : String)
    {
        let alert : UIAlertController = UIAlertController(title: "Message", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
            (action) in
            alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func storeWorksheet() {
        if self.workSheetData.count > 0 {
            for key in self.workSheetData {
                    workSheetNames.append((key["worksheetName"] as? String)!)
                    workSheetIds.append((key["worksheetId"] as? String)!)
                    if let thres = key["threshold"] as? Int {
                        workSheetThreshold.append("\(thres)")
                    }
                }
            }
        
        dropDownWorkSheet.dataSource = workSheetNames
        }
    
}

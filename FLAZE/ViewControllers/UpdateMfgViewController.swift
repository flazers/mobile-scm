//
//  UpdateMfgViewController.swift
//  FLAZE
//
//  Created by swamy kottu on 5/21/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import UIKit

protocol UpdateMfgActionDelegate {
    func updateClick(mfgforcast:String, Comment:String)
}

class UpdateMfgViewController: UIViewController {
    
    @IBOutlet weak var mfgTextField: UITextField!
    @IBOutlet weak var commentsTextField: UITextField!
    
    var updateMfgDelegate:UpdateMfgActionDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func updateClicked(sender: AnyObject) {
        updateMfgDelegate.updateClick(mfgTextField.text!, Comment:commentsTextField.text!)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}

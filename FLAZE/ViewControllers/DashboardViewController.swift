//
//  DashboardViewController.swift
//  FLAZE
//
//  Created by Sista, Siva Rama Krishna on 5/20/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import UIKit

class DashboardTableViewCell:UITableViewCell {
    
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var priorityLabel: UILabel!
}

class DashboardViewController: UIViewController, UIWebViewDelegate,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sidebarButton: UIBarButtonItem!
    
    var reachability: Reachability?
    var loadingView : UIView?
    
    // url to load
    let urlString : String = "http://mavlracorp.ddns.net:8080/Flaze/images/dashboard.html"
    
    // outlet - webview
    @IBOutlet var dashboardWebView: UIWebView!
    
    // outlet - activity indicator i.e. spinner
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    var topItems: [Dashboard] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        self.title = "Dashboard"
        
        
        self.setNeedsStatusBarAppearanceUpdate()
        
        self.sidebarButton.target = self.revealViewController()
        self.sidebarButton.action = "revealToggle:"
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        self.serviceCall("")
        
        // set webview delegate
        self.dashboardWebView.delegate = self
        
        // fit content within screen size.
        self.dashboardWebView.scalesPageToFit = true
        
        // start spinner
        self.spinner.startAnimating()

        let url = NSURL (string: urlString)
        let urlRequest = NSURLRequest(URL: url!)
        self.dashboardWebView.loadRequest(urlRequest)

    }

    // MARK: - Web view delegate function
    func webViewDidFinishLoad(webView: UIWebView) {
        self.spinner.stopAnimating()
    }
    
    func connection(connection: NSURLConnection, canAuthenticateAgainstProtectionSpace protectionSpace: NSURLProtectionSpace) -> Bool{
        print("canAuthenticateAgainstProtectionSpace method Returning True")
        return true
    }
    
    func connection(connection: NSURLConnection, didReceiveAuthenticationChallenge challenge: NSURLAuthenticationChallenge){
        
        print("did autherntcationchallenge = \(challenge.protectionSpace.authenticationMethod)")
        
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust  {
            print("send credential Server Trust")
            let credential = NSURLCredential(forTrust: challenge.protectionSpace.serverTrust!)
            challenge.sender!.useCredential(credential, forAuthenticationChallenge: challenge)
            
        }else if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodHTTPBasic{
            print("send credential HTTP Basic")
            let defaultCredentials: NSURLCredential = NSURLCredential(user: "username", password: "password", persistence:NSURLCredentialPersistence.ForSession)
            challenge.sender!.useCredential(defaultCredentials, forAuthenticationChallenge: challenge)
            
        }else if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodNTLM{
            print("send credential NTLM")
            
        } else{
            challenge.sender!.performDefaultHandlingForAuthenticationChallenge!(challenge)
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRectMake(0, 10, self.view.frame.size.width, 20))
        view.backgroundColor = UIColor.darkGrayColor()
        let sectionTitleLabel = UILabel(frame: view.frame)
        sectionTitleLabel.backgroundColor = UIColor.clearColor()
        sectionTitleLabel.textColor = UIColor.whiteColor()
        sectionTitleLabel.font = UIFont.boldSystemFontOfSize(14)
        sectionTitleLabel.text = "Top Ten Variance Items"
        sectionTitleLabel.textAlignment = .Center
        view.addSubview(sectionTitleLabel)
        return view
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:DashboardTableViewCell = self.tableView.dequeueReusableCellWithIdentifier("cell")! as! DashboardTableViewCell
        
        cell.itemLabel?.text = "Item: " + self.topItems[indexPath.row].item!
        cell.dateLabel?.text =  "Variance (%): " + "\(self.topItems[indexPath.row].variancePercent! as Int)"
        cell.priorityLabel?.text =  ""//"Priority: " + "\(self.topItems[indexPath.row].priority! as Int )"
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        return cell
    }
    
    func serviceCall(serviceName : String)
    {
        do {
            let reachability = try Reachability.reachabilityForInternetConnection()
            self.reachability = reachability
        } catch ReachabilityError.FailedToCreateWithAddress(let address) {
            print("Unable to create\nReachability with address:\n\(address)")
            return
        } catch {}
        
        if let reachability = reachability {
            if reachability.isReachable() {
                loadingView = LoadingView.instanceFromNib()
                self.view.addSubview(loadingView!)
                let views = Dictionary(dictionaryLiteral:("loadingView",loadingView!))
                loadingView!.translatesAutoresizingMaskIntoConstraints = false
                //Horizontal constraints
                let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[loadingView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
                self.view.addConstraints(horizontalConstraints)
                
                //Vertical constraints
                let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[loadingView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
                self.view.addConstraints(verticalConstraints)
                self.performSelector("dashboardData:", withObject: "", afterDelay: 0.1)
            } else {
                let alert : UIAlertController = UIAlertController(title: "Message", message: "Please check internet connectivity", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
                    (action) in
                    alert.dismissViewControllerAnimated(true, completion: nil)
                }))
                
                self.loadingView?.removeFromSuperview()
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }

    
    func dashboardData(serviceName : String){
        let dashboardData: simpleCallback = { (data: [String: AnyObject]) in
            dispatch_async(dispatch_get_main_queue(), {
                if data.count > 0 {
                    if let result = data["planners"] as? [[String: AnyObject]] {
                        self.parseData(result)
                    }
                }else {
                    self.errormessage()
                }
                
                self.loadingView?.removeFromSuperview()
            })
            
        }
        
        ServicesRequest.dashboardData(dashboardData)
    }
    
    func parseData(dashboardData: [[String: AnyObject]]) {
        for data in dashboardData {
            if let items = Dashboard(json: data) {
                topItems.append(items)
            }
        }
        
        tableView.reloadData()
    }
    
    func errormessage() {
        let alert : UIAlertController = UIAlertController(title: "Message", message: "Please try again", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
            (action) in
            alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    

 }

//
//  SOPTopThresholdViewController.swift
//  FLAZE
//
//  Created by Sista, Siva Rama Krishna on 5/17/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import UIKit

class SOPTopTableViewCell:UITableViewCell {
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var organizationLabel: UILabel!
    @IBOutlet weak var varianceLabel: UILabel!
}

class SOPTopThresholdViewController: UIViewController {
    @IBOutlet weak var sidebarButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    var mfgsData  = [[String:AnyObject]]()

    var reachability: Reachability?
    var loadingView : UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Supply & Demand Overview"
        self.setNeedsStatusBarAppearanceUpdate()
        self.sidebarButton.target = self.revealViewController()
        self.sidebarButton.action = "revealToggle:"
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        self.serviceCall("")
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mfgsData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:SOPTopTableViewCell = self.tableView.dequeueReusableCellWithIdentifier("cell")! as! SOPTopTableViewCell
        
        let thresholdData = mfgsData[indexPath.row]
        
        cell.organizationLabel.text! = "Organization: " + "\(thresholdData["organization"]!["name"] as! String)"
        cell.itemLabel.text! = "Item: " + "\(thresholdData["item"]!["name"] as! String)"
        
        let varianceStr = thresholdData["variance"]!["name"] as! Int
        cell.varianceLabel.text! = "Variance: " + String(varianceStr)

        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let detailView = self.storyboard?.instantiateViewControllerWithIdentifier("SOPCollectionViewController") as? SOPCollectionViewController
        detailView!.yearlyData = mfgsData[indexPath.row]
        self.navigationController?.pushViewController(detailView!, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
    }
    
    func serviceCall(serviceName : String)
    {
        do {
            let reachability = try Reachability.reachabilityForInternetConnection()
            self.reachability = reachability
        } catch ReachabilityError.FailedToCreateWithAddress(let address) {
            print("Unable to create\nReachability with address:\n\(address)")
            return
        } catch {}
        
        if let reachability = reachability {
            if reachability.isReachable() {
                loadingView = LoadingView.instanceFromNib()
                self.view.addSubview(loadingView!)
                let views = Dictionary(dictionaryLiteral:("loadingView",loadingView!))
                loadingView!.translatesAutoresizingMaskIntoConstraints = false
                //Horizontal constraints
                let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[loadingView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
                self.view.addConstraints(horizontalConstraints)
                
                //Vertical constraints
                let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[loadingView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
                self.view.addConstraints(verticalConstraints)
                self.performSelector("topThresholdData:", withObject: "", afterDelay: 0.1)
            } else {
                let alert : UIAlertController = UIAlertController(title: "Message", message: "Please check internet connectivity", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
                    (action) in
                    alert.dismissViewControllerAnimated(true, completion: nil)
                }))
                
                self.loadingView?.removeFromSuperview()
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func topThresholdData(serviceName : String){
        let topThresholdData: simpleCallback = { (data: [String: AnyObject]) in
            dispatch_async(dispatch_get_main_queue(), {
                if data.count > 0 {
                        let sd = data["supplyDemand"]!
                        self.mfgsData = (sd as? [[String:AnyObject]])!
                        // self.mfgsData = forecastdata
                        self.tableView.reloadData()
                }else {
                    self.errormessage()
                }
                
                self.loadingView?.removeFromSuperview()
            })
            
        }
        
        ServicesRequest.sdTopThreshold(topThresholdData)
    }
    
    func errormessage() {
        let alert : UIAlertController = UIAlertController(title: "Message", message: "Please try again", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
            (action) in
            alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
}
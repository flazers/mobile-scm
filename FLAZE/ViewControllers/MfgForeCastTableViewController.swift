//
//  MfgForeCastTableViewController.swift
//  FLAZE
//
//  Created by swamy kottu on 5/21/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import UIKit

class MfgForeCastTableViewController: UIViewController , UpdateMfgActionDelegate{
    @IBOutlet weak var alphaView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sidebarButton: UIBarButtonItem!
    @IBOutlet weak var containerView: UIView!

    var reachability: Reachability?
    var loadingView : UIView?
    var mfgsData  = [[String:AnyObject]]()
    var customVC : UpdateMfgViewController?
    var updateRow : Int = -1
    var alphaViewFlag = false
    var updatedComments = ""
    var updatedMfgValue = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Manfacturer Forecast"
        self.setNeedsStatusBarAppearanceUpdate()
        self.sidebarButton.target = self.revealViewController()
        self.sidebarButton.action = "revealToggle:"
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.customVC = UpdateMfgViewController(nibName: "UpdateMfgViewController", bundle: nil)
        self.customVC?.updateMfgDelegate = self
        let tap = UITapGestureRecognizer(target: self, action: "dismissSearch")
        self.alphaView.addGestureRecognizer(tap)
        self.serviceCall("getMfgs")
    }
    
    func dismissSearch() -> Void {
        alphaViewFlag = true
        self.editMfg()
    }
    
    func updateClick(mfgforcast:String, Comment:String) {
        alphaViewFlag = true
        self.editMfg()
        self.updatedComments = Comment
        self.updatedMfgValue = mfgforcast
        self.serviceCall("sendMfgs")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mfgsData.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
         var cell : MfgForeCastTableViewCell? = tableView.dequeueReusableCellWithIdentifier("cell") as? MfgForeCastTableViewCell

        // Configure the cell...
        if cell == nil {
            let toplevelObjects : NSArray = NSBundle.mainBundle().loadNibNamed("MfgForeCastTableViewCell", owner: self, options: nil)
            cell = toplevelObjects[0] as? MfgForeCastTableViewCell
        }
        
        let forecastData = mfgsData[indexPath.row]
        cell!.mfgForeCastValue.text! = "\(forecastData["mfgApprovedForecast"]!)"
        cell!.saleDateValue.text! = "\(forecastData["salesDate"]!)"
        cell!.orgValue.text! = "\(forecastData["organization"]!)"
        let mfgComments  = forecastData["mfgComments"] as! String
        
        if (mfgComments != "null") {
            cell!.mfgComments.text! = mfgComments
        }
        
        cell!.forecastValue.text! = "\(forecastData["finalForecast"]!)"
        cell!.itemValue.text! = "\(forecastData["item"]!)"
        cell!.approvedButton.tag = indexPath.row
        cell!.approvedButton.addTarget(self, action: "checkMarkSelected:", forControlEvents: .TouchUpInside)
        cell!.approvedButton.layer.cornerRadius = 4.0
        if "\(forecastData["mfgComments"]!)".isEmpty{
            cell!.mfgComments.text! = ""
        }
        
        cell?.selectionStyle = .None
        return cell!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 112
    }

     func checkMarkSelected(sender: CheckMark) {
        sender.isselected = !sender.isselected
        sender.setselected()
     }
    
    // Override to support conditional editing of the table view.
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    // Override to support editing the table view.
     func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .None {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }
    
     func tableView(tableView: UITableView, shouldIndentWhileEditingRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
     func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        let editaction = UITableViewRowAction(style: .Default, title: "Edit", handler: { (UITableViewRowAction, NSIndexPath) in
            self.editing = false
            self.updateRow = indexPath.row
            self.performSelector("afterDelay", withObject: nil, afterDelay: 0.1)
        })
        
        return [editaction]
    }
    
    func afterDelay() -> Void {
        self.editMfg()
    }
    
    func editMfg() -> Void {
        if alphaViewFlag == false {
            self.alphaView.hidden = false
            alphaViewFlag = true
            self.view.bringSubviewToFront(self.alphaView)
            self.view.bringSubviewToFront(self.containerView)
            self.addChildViewController(customVC!)
            customVC?.view.translatesAutoresizingMaskIntoConstraints = true
            customVC?.view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth , UIViewAutoresizing.FlexibleHeight]
            //2. Define the detail controller's view size
            customVC?.view.frame = self.containerView.bounds
            self.containerView.addSubview((customVC?.view)!)
            self.customVC?.view.alpha = 1.0
            self.alphaView.alpha = 0.3
            self.customVC?.didMoveToParentViewController(self)
            let transientTransform : CGAffineTransform = CGAffineTransformMakeScale(0.5, 0.5)
            self.customVC?.view.transform = transientTransform;
            
            let midX : CGFloat = CGRectGetMidX(self.view.bounds)
            let midY : CGFloat = CGRectGetMidY(self.view.bounds)
            self.alphaView.center = CGPointMake(midX, midY);
            let forecastData = mfgsData[self.updateRow]
            self.customVC?.commentsTextField.text = "\(forecastData["mfgComments"]!)"
            self.customVC?.mfgTextField.text = "\(forecastData["mfgApprovedForecast"]!)"
            UIView.animateWithDuration(0.3, animations: {
                self.customVC?.view.transform = CGAffineTransformMakeScale(1.0, 1.0);
                self.containerView.hidden = false
                }, completion: {
                    (success) in
            })
        }else {
            self.alphaView.hidden = true
            alphaViewFlag = false
            self.customVC?.willMoveToParentViewController(nil)
            let  shrinkTransform : CGAffineTransform = CGAffineTransformMakeScale(0.1, 0.1)
            UIView.animateWithDuration(0.3, animations: {
                self.customVC?.view.transform = shrinkTransform
                }, completion: {
                    (success) in
                    self.customVC?.view.removeFromSuperview()
                    self.customVC?.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
                    self.customVC?.removeFromParentViewController()
                    self.containerView.hidden = true
            })
        }
    }
    
    func serviceCall(serviceName : String) {
        do {
            let reachability = try Reachability.reachabilityForInternetConnection()
            self.reachability = reachability
        } catch ReachabilityError.FailedToCreateWithAddress(let address) {
            print("Unable to create\nReachability with address:\n\(address)")
            return
        } catch {}
        
        if let reachability = reachability {
            if reachability.isReachable() {
                loadingView = LoadingView.instanceFromNib()
                self.view.addSubview(loadingView!)
                let views = Dictionary(dictionaryLiteral:("loadingView",loadingView!))
                loadingView!.translatesAutoresizingMaskIntoConstraints = false
                //Horizontal constraints
                let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[loadingView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
                self.view.addConstraints(horizontalConstraints)
                
                //Vertical constraints
                let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[loadingView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
                self.view.addConstraints(verticalConstraints)
                self.performSelector("mfgForeCastServiceCall:", withObject: serviceName, afterDelay: 0.1)
            }else {
                let alert : UIAlertController = UIAlertController(title: "Message", message: "Please check internet connectivity", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
                    (action) in
                    alert.dismissViewControllerAnimated(true, completion: nil)
                }))
                
                self.loadingView?.removeFromSuperview()
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func mfgForeCastServiceCall(serviceName : String) {
        let getMfgs: simpleCallback = { (data: Dictionary<String, AnyObject>) in
            dispatch_async(dispatch_get_main_queue(), {
                if data.count > 0 {
                   let forecastdata = data["forecast"]!
                    self.mfgsData = (forecastdata as? [[String:AnyObject]])!
                     self.tableView.reloadData()
                }else {
                    self.showAlertMessage("Please try again after some time")
                }
                
                self.loadingView?.removeFromSuperview()
            })
        }
        
        let sendMfgs: simpleCallback = { (data: Dictionary<String, AnyObject>) in
            dispatch_async(dispatch_get_main_queue(), {
                if data.count > 0 {
                    self.loadingView?.removeFromSuperview()
                    self.serviceCall("getMfgs")
                }else {
                    self.showAlertMessage("Please try again after some time")
                }
                
                self.loadingView?.removeFromSuperview()
            })
        }
        
        if serviceName == "getMfgs" {
            ServicesRequest.getMfgForecast("611", callback: getMfgs)
        }else if serviceName == "sendMfgs" {
            let forecastData = mfgsData[self.updateRow]
            let rowId = "\(forecastData["rowId"]!)"
           
            ServicesRequest.updateMfgForecast("611", rowId: rowId, mfgApprovalFlag: Int(self.updatedMfgValue)  > 0 ? "2" : "1", mfgApproved: self.updatedMfgValue, comments: self.updatedComments, callback: sendMfgs)
        }
    }
    
    func showAlertMessage(message : String)
    {
        let alert : UIAlertController = UIAlertController(title: "Message", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
            (action) in
            alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }

}

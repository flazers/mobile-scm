//
//  PlannerSelectViewController.swift
//  FLAZE
//
//  Created by Sista, Siva Rama Krishna on 5/19/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import UIKit

class PlannerSelectViewController: UIViewController, UITextFieldDelegate, PickerDelegateMethods {
    @IBOutlet weak var plannerTextField: UITextField!
    @IBOutlet weak var alphaView: UIView!
    @IBOutlet weak var sendButton: UIButton!


    var customPicker : CustomPickerView?
    var reachability: Reachability?
    var loadingView : UIView?
    var plannerNames:[Planner] = []
    var selectedIndex = -1

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Planner View"
        self.sendButton.layer.cornerRadius  = 6.0
        self.serviceCall("")
    }
    
    
    func showPicker() -> Void {
        self.alphaView.hidden = false
        self.view.bringSubviewToFront(self.alphaView)
            self.customPicker = CustomPickerView.instanceFromNib()
            
            self.customPicker?.data = plannerNames.map { $0.userName! }
            self.customPicker?.pickerDelegate = self
            // position view offscreen in the direction that is should appear from
            let originalFrame : CGRect = CGRectMake( 0,
                self.view.frame.size.height - 266,
                CGRectGetWidth(self.view!.frame),
                266)
            self.customPicker!.frame = CGRectMake( CGRectGetMinX(self.customPicker!.frame),
                self.view.frame.size.height,
                CGRectGetWidth(self.customPicker!.frame),
                CGRectGetHeight(self.customPicker!.frame))
            
            self.view.addSubview(self.customPicker!)
            
            UIView.animateWithDuration(0.4, animations: {
                self.customPicker?.frame = originalFrame
            })
        
    }
    
    func doneOrCancelClick(value : String)
    {
        
        let indexSelected = self.customPicker?.pickerView.selectedRowInComponent(0)
        if value == "done" {
            self.plannerTextField.text = plannerNames[indexSelected!].userName
            self.selectedIndex = indexSelected!
        }
        self.alphaView.hidden = true
        self.view.sendSubviewToBack(self.alphaView)
        let originalFrame : CGRect = CGRectMake( 0,
            self.view.frame.size.height,
            CGRectGetWidth(self.view!.frame),
            266)
        UIView.animateWithDuration(0.4, animations: {
            self.customPicker?.frame = originalFrame
        })
        self.customPicker?.removeFromSuperview()
    }
    
    
    func serviceCall(serviceName : String)
    {
        do {
            let reachability = try Reachability.reachabilityForInternetConnection()
            self.reachability = reachability
        } catch ReachabilityError.FailedToCreateWithAddress(let address) {
            print("Unable to create\nReachability with address:\n\(address)")
            return
        } catch {}
        
        if let reachability = reachability {
            if reachability.isReachable() {
                loadingView = LoadingView.instanceFromNib()
                self.view.addSubview(loadingView!)
                let views = Dictionary(dictionaryLiteral:("loadingView",loadingView!))
                loadingView!.translatesAutoresizingMaskIntoConstraints = false
                //Horizontal constraints
                let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[loadingView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
                self.view.addConstraints(horizontalConstraints)
                
                //Vertical constraints
                let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[loadingView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
                self.view.addConstraints(verticalConstraints)
                self.performSelector("plannerData:", withObject: "", afterDelay: 0.1)
            } else {
                let alert : UIAlertController = UIAlertController(title: "Message", message: "Please check internet connectivity", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
                    (action) in
                    alert.dismissViewControllerAnimated(true, completion: nil)
                }))
                
                self.loadingView?.removeFromSuperview()
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func viewTapped() {
        view.endEditing(false)
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        showPicker()
        return false
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func plannerData(serviceName : String){
        let plannerData: simpleCallback = { (data: [String: AnyObject]) in
            dispatch_async(dispatch_get_main_queue(), {
                if data.count > 0 {
                    if let result = data["planners"] as? [[String: AnyObject]] {
                        self.parseData(result)
                    }
                }else {
                    self.errormessage()
                }
                
                self.loadingView?.removeFromSuperview()
            })

        }
        
        ServicesRequest.plannersData(plannerData)
    }
    
    func parseData(plannerData: [[String: AnyObject]]) {
        
        for planners in plannerData {
            if let user = Planner(json: planners) {
                plannerNames.append(user)
            }
        }
    }

    func errormessage() {
        let alert : UIAlertController = UIAlertController(title: "Message", message: "Please try again", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
            (action) in
            alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func sendAction(sender: AnyObject) {
        if (plannerTextField.text?.characters.count > 0) {
            let alert : UIAlertController = UIAlertController(title: "Message", message: "Send Successful", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
                (action) in
                alert.dismissViewControllerAnimated(true, completion: nil)
            }))
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
}

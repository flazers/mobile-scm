//
//  AlertsdetailViewController.swift
//  FLAZE
//
//  Created by Sista, Siva Rama Krishna on 4/17/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import UIKit

class AlertsDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var alerts:Alerts?
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var alertNameLabel: UILabel!
    let displaySOP:Bool = true
    @IBOutlet weak var sopButton: UIButton!
    @IBOutlet weak var fwdButton: UIButton!
    
    var headerData : [String:String] = [:]
    var detailOnlyItems : [String:String] = [:]
    var detailItems : [String:String] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Alerts Detail"
        
        self.sopButton.layer.cornerRadius  = 6.0
        self.fwdButton.layer.cornerRadius  = 6.0
        
        alertNameLabel.textAlignment = NSTextAlignment.Left
        alertNameLabel.font = UIFont.boldSystemFontOfSize(18)
        alertNameLabel.textColor = UIColor.blackColor()
        alertNameLabel.text = alerts?.alertName["name"]
        
        headerData = (alerts?.getHeaders())!
        detailOnlyItems = (alerts?.getDetailOnlyItems())!
        headerData.merge(detailOnlyItems)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return headerData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : AlertDetailTableViewCell? = tableView.dequeueReusableCellWithIdentifier("cell") as? AlertDetailTableViewCell
        
        if cell == nil
        {
            let toplevelObjects : NSArray = NSBundle.mainBundle().loadNibNamed("AlertDetailTableViewCell", owner: self, options: nil)
            cell = toplevelObjects[0] as? AlertDetailTableViewCell
        }
        
        cell?.backgroundColor = UIColor.clearColor()
        
        var key : String = ""
        var value : String = ""
        var  i = 0
        for (Key,Value) in headerData {
            
            if Key == "alertName" {
                continue
            }
            key = Key
            value = Value
            if  i == indexPath.row {
                break
            }
           i += 1
        }
        
        if i < headerData.count - 1{
            cell?.keyLabel?.text = key.stringFromCamelCase() + ":"
            cell?.valueLabel?.text = value
            cell?.selectionStyle = .None
        }
        
        return cell!
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
        if cell.respondsToSelector(Selector("setSeparatorInset:")){
            cell.separatorInset = UIEdgeInsetsZero
        }
        
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")){
            cell.preservesSuperviewLayoutMargins = false
        }
        
        if cell.respondsToSelector(Selector("setLayoutMargins:")){
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    @IBAction func getSOP(sender: AnyObject) {
        
        
        let detailView = self.storyboard?.instantiateViewControllerWithIdentifier("SOPMonthly") as? SOPMonthCollectionViewController
        
        detailView!.items = alerts?.item["name"]
        detailView!.organization = alerts?.organization["name"]
        
        self.navigationController?.pushViewController(detailView!, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
    }
    
    @IBAction func fwdAlert(sender: AnyObject) {
        
        let storyboard = UIStoryboard(name: "PlannerSelectViewController", bundle: nil)
        let plannerView = storyboard.instantiateViewControllerWithIdentifier("PlannerSelectViewController") as? PlannerSelectViewController
        
        self.navigationController?.pushViewController(plannerView!, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
    }

}

extension String {
    func stringFromCamelCase() -> String {
        var string = self
        string = string.stringByReplacingOccurrencesOfString("Percentage", withString: " (%)")
        string = string.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        string = string.stringByReplacingOccurrencesOfString("([a-z])([A-Z])", withString: "$1 $2", options: NSStringCompareOptions.RegularExpressionSearch, range: Range<String.Index>(start: string.startIndex, end: string.endIndex))
        string.replaceRange(startIndex...startIndex, with: String(self[startIndex]).capitalizedString)
        return string
    }
}

extension Dictionary {
    mutating func merge<K, V>(dict: [K: V]){
        for (k, v) in dict {
            self.updateValue(v as! Value, forKey: k as! Key)
        }
    }
}
    




//
//  ReportType.swift
//  FLAZE
//
//  Created by Sista, Siva Rama Krishna on 3/29/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import Foundation

class ReportType: NSObject {
    var reportID: String?
    var reportName: String?
    
    override init() {
        super.init()
    }
    
    required init?(json: AnyObject) {
        super.init()
        
        guard let dict = json as? [String: AnyObject] else {
            return nil
        }
        
        if let id = dict["reportID"] as? String {
            reportID = id
        }
        
        if let name = dict["reportName"] as? String {
            reportName = name
        }
        
    }
}
//
//  ViewController.swift
//  SalesForce
//
//  Created by SRK-SK on 3/9/16.
//  Copyright © 2016 com.SRK. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var logoImgView: UIImageView!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    let dataConfig = DataConfiguration.sharedInstance
    let userDefaults = NSUserDefaults.standardUserDefaults()
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    var reachability: Reachability?
    var loadingView : UIView?
    @IBOutlet weak var rememberButton: CheckMark!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setNeedsStatusBarAppearanceUpdate()
        self.userNameTextField.layer.cornerRadius = 6.0;
        //self.userNameTextField.text = "naga"
        
        self.passwordTextField.layer.cornerRadius = 6.0;
        //self.passwordTextField.text = "welcome123"
        
        //https://medium.com/simple-swift-programming-tips/how-to-set-padding-for-uitextfield-in-swift-2f830d131f40#.rmjpnno3t
        let paddingView1 = UIView(frame: CGRectMake(0, 0, 15, self.userNameTextField.frame.height))
        userNameTextField.leftView = paddingView1
        userNameTextField.leftViewMode = .Always
        let paddingView2 = UIView(frame: CGRectMake(0, 0, 15, self.passwordTextField.frame.height))
        passwordTextField.leftView = paddingView2
        passwordTextField.leftViewMode = .Always
        self.loginButton.layer.cornerRadius  = 6.0
        let remeberFlag : Bool = self.userDefaults.boolForKey("RememberMe")
        let useLoginStaus : Bool = self.userDefaults.boolForKey("loginStatus")
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isDeviceDataSent")
        
        if remeberFlag == true && useLoginStaus == true {
            self.performSegueWithIdentifier("revealController", sender: self)
        }else {
            self.serviceCall("getLogoImage")
        }
    }
    
    @IBAction func showNotificationView(sender: AnyObject)
    {
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.barTintColor = CommonUiStyle.navbarColor()
        self.navigationController?.navigationBar.backgroundColor = CommonUiStyle.navbarColor()
    }

    @IBAction func loginToSandbox(sender: AnyObject) {
        self.serviceCall("checkUserLogin") // Note remove this to go to home screen
        
    }
    
    @IBAction func checkMarkSelected(sender: CheckMark) {
        
        sender.isselected = !sender.isselected
        self.userDefaults.setBool(sender.isselected, forKey: "RememberMe")
        sender.setselected()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return [.Portrait]
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        self.heightConstraint.constant = 0
        textField.layer.borderColor = CommonUiStyle.navbarColor().CGColor
        textField.layer.borderWidth = 1.0;
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        self.heightConstraint.constant = 164
        textField.layer.borderColor = UIColor.lightGrayColor().CGColor
        textField.layer.borderWidth = 1.0;

    }

    func serviceCall(serviceName : String) {
        do {
            let reachability = try Reachability.reachabilityForInternetConnection()
            self.reachability = reachability
        } catch ReachabilityError.FailedToCreateWithAddress(let address) {
            print("Unable to create\nReachability with address:\n\(address)")
            return
        } catch {}
        
        if let reachability = reachability {
            if reachability.isReachable() {
                loadingView = LoadingView.instanceFromNib()
                self.view.addSubview(loadingView!)
                let views = Dictionary(dictionaryLiteral:("loadingView",loadingView!))
                loadingView!.translatesAutoresizingMaskIntoConstraints = false
                
                //Horizontal constraints
                let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[loadingView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
                self.view.addConstraints(horizontalConstraints)
                
                //Vertical constraints
                let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[loadingView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
                self.view.addConstraints(verticalConstraints)
                
                self.performSelector("callafter:", withObject: serviceName, afterDelay: 0.1)
            }else {
                let alert : UIAlertController = UIAlertController(title: "Message", message: "Please check internet connectivity", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
                    (action) in
                    alert.dismissViewControllerAnimated(true, completion: nil)
                }))
                self.loadingView?.removeFromSuperview()
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func callafter(serviceName : String){
        let logoImage: dataCallback = { (data : NSData?) in
            dispatch_async(dispatch_get_main_queue(), {
                if data?.length > 0 {
                   self.logoImgView.image = UIImage(data: data!)
                }else {
                   self.errormessage()
                }
                
                self.loadingView?.removeFromSuperview()
            })
        }
        
        let userLogin: simpleCallback = { (data: Dictionary<String, AnyObject>) in
            dispatch_async(dispatch_get_main_queue(), {
                if data.count > 0 {
                    if let result = data["authentication"] as? String {
                        if result == "success" {
                            self.userDefaults.setBool(true, forKey: "loginStatus")
                            let userID : Int = (data["id"] as? Int)!
                            let userIDString = String(userID)
                            self.userDefaults.setObject(userIDString, forKey: "userID")
                            self.userDefaults.setObject(data["name"], forKey: "userName")
                            self.userDefaults.setObject(data["email"], forKey: "userEmail")
                            self.performSegueWithIdentifier("revealController", sender: self)
                        } else {
                            self.userDefaults.setBool(false, forKey: "loginStatus")
                            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isDeviceDataSent")
                            self.errormessage()
                        }
                    }
                }else {
                    self.userDefaults.setBool(false, forKey: "loginStatus")
                    NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isDeviceDataSent")
                    self.performSegueWithIdentifier("revealController", sender: self)
                }
                
                self.loadingView?.removeFromSuperview()
            })
        }
        
        if serviceName == "getLogoImage" {
            ServicesRequest.loginImage(logoImage)
        }
        
        if serviceName == "checkUserLogin" {
            ServicesRequest.userLogin(self.userNameTextField.text!, password: self.passwordTextField.text!, callback: userLogin)
        }
    }
    
    func errormessage() {
        let alert : UIAlertController = UIAlertController(title: "Message", message: "Please try again", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: {
            (action) in
            alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
}


//
//  CheckMark.swift
//  FLAZE
//
//  Created by swamy kottu on 3/23/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import UIKit
@IBDesignable
class CheckMark: UIButton {
    let bezierPath = UIBezierPath()
    var isselected : Bool = false
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        if isselected
        {
            bezierPath.moveToPoint(CGPointMake(5.5, 10.75))
            bezierPath.addCurveToPoint(CGPointMake(12.25, 19.5), controlPoint1: CGPointMake(12.25, 19.5), controlPoint2: CGPointMake(12.25, 19.5))
            bezierPath.addLineToPoint(CGPointMake(19, 4.5))
            UIColor.grayColor().setStroke()
            bezierPath.lineWidth = 2.5
            bezierPath.stroke()
        }
        else
        {
            bezierPath.removeAllPoints()
        }
        
    }
    
    func setselected()
    {
        self.setNeedsDisplay()
    }
    

}

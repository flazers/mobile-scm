//
//  LoadingView.swift
//  Ahad
//
//  Created by swamy on 19/10/15.
//  Copyright © 2015 com.swamy. All rights reserved.
//

import UIKit

class LoadingView: UIView {
    
    @IBOutlet var view: UIView!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "LoadingView", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! UIView
    }

    
}

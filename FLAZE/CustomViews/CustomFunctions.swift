//
//  CustomFunctions.swift
//  Connect
//
//  Created by SRK-SK on 1/23/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import Foundation

class CustomFunctions {
    
  static func randomStringWithLength (with: NSMutableString,len : Int) -> NSString {
        
        let finalString = with
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
    
        for _ in 0 ..< len
        {
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.characterAtIndex(Int(rand)))
        }
        finalString.appendString(randomString as String)
        return finalString
    }
    
   static func currentDateString() -> NSString{
        let now  = NSDate()
        let formatter = NSDateFormatter()
        
        // No date style or time style defined:
        // The result for the line below is
        // ""
        
        // Only the date style is defined:
        formatter.dateStyle = .MediumStyle
        formatter.dateFormat = "dd-MMM-YYYY HH:mm:ss"
        
        let date = formatter.stringFromDate(now)
        
        return date
    }
    
}


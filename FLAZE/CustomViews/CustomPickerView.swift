//
//  CustomPickerView.swift
//  FLAZE
//
//  Created by swamy kottu on 3/27/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import UIKit

protocol PickerDelegateMethods{
    func doneOrCancelClick(value : String)
}

class CustomPickerView: UIView , UIPickerViewDelegate, UIPickerViewDataSource{
    
    var pickerDelegate : PickerDelegateMethods?
    var data = ["Dynamic 1", "Dynamic 2", "Dynamic 3"]
    @IBOutlet weak var pickerView: UIPickerView!

    @IBAction func doneClicked(sender: AnyObject)
    {
        pickerDelegate?.doneOrCancelClick("done")
    }

    @IBAction func cancelClicked(sender: AnyObject)
    {
        pickerDelegate?.doneOrCancelClick("cancel")
    }
    
    class func instanceFromNib() -> CustomPickerView {
        return UINib(nibName: "CustomPickerView", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! CustomPickerView
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
       return 1
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        
        let label = UILabel(frame: CGRectMake(0, 10, self.frame.size.width, 30))
        label.font = UIFont.systemFontOfSize(15)
        label.textAlignment = NSTextAlignment.Center
        label.text = data[row]
        return label
        
    }
    
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 44.0
    }
}

//
//  MfgForeCastTableViewCell.swift
//  FLAZE
//
//  Created by swamy kottu on 5/21/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import UIKit

class MfgForeCastTableViewCell: UITableViewCell {
    @IBOutlet weak var mfgForeCastValue: UILabel!
    @IBOutlet weak var saleDateValue: UILabel!
    @IBOutlet weak var orgValue: UILabel!
    @IBOutlet weak var mfgComments: UILabel!
    @IBOutlet weak var forecastValue: UILabel!
    @IBOutlet weak var itemValue: UILabel!

    @IBOutlet weak var approvedButton: CheckMark!
    override func awakeFromNib() {
              super.awakeFromNib()
        // Initialization code
        
        approvedButton.isselected = true
        approvedButton.setselected()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

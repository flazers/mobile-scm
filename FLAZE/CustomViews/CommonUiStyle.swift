//
//  CommonUiStyle.swift
//  Ahad
//
//  Created by SRK-SK on 15/10/15.
//  Copyright © 2015 com.swamy. All rights reserved.
//

import Foundation
import UIKit

class CommonUiStyle {
    static let DOMAIN = "http://mavlracorp.ddns.net:8080/Flaze/rest/"
    
    static let LOGIN_IMAGE = DOMAIN + "UserService/loginImage180"
    static let USER_LOGIN = DOMAIN + "UserService/authenticate?username="
    static let DEFAULT_SETUP = DOMAIN + "SetupService/defaultSetup"
    static let SET_USER_REPORT = DOMAIN + "ReportService/setReportSettings"
    static let GET_USER_NOTIFICATIONS = DOMAIN + "AlertService/alertTypes"
    static let UPDATE_NOTIFICATIONS = DOMAIN + "NotificationService/updateSettings"
    static let DEVICE_TOKEN = DOMAIN + "UserService/userStatus?userId="
    static let DEFAULT_ALERTS = DOMAIN + "AlertService/defaultAlerts"
    static let FILTER_ALERTS = DOMAIN + "AlertService/filterByAlertType"
    static let GET_WORKSHEET_BY_ALERTS = DOMAIN + "WorksheetService/worksheetsByAlertType"
    static let SEARCH_ALERTS = DOMAIN + "AlertService/searchAlerts"
    static let SUPPLY_DEMAND = DOMAIN + "SupplyDemandService/defaultSupplyDemand"
    static let PLANNERS = DOMAIN + "SupplyDemandService/getPlanners"
    static let TOP_TEN_ITEMS = DOMAIN + "DashboardService/getTopTenItems"
    static let MFGFORECAST = DOMAIN + "SupplyDemandService/getManufacturingForecast"
    static let UPDATEMFGFORECAST = DOMAIN + "SupplyDemandService/updateManufacturingForecast"
    
    static let SPTOPTHRESHOLD = DOMAIN + "SupplyDemandService/supplyDemandHeader"
    static let SPDETAILS = DOMAIN + "SupplyDemandService/supplyDemandDetails"





    static func navbarColor() -> UIColor{
        return UIColor(red: 88/255, green: 116/255, blue: 135/255, alpha: 1.0)
    }
    
    static func btnColor() -> UIColor{
        return UIColor(red: 240.0/255.0, green: 91.0/255.0, blue: 47.0/255.0, alpha: 1.0)
    }
}

//
//  SOPCollectionViewController.swift
//  CustomCollectionLayout
//
//  Created by JOSE MARTINEZ on 15/12/2014.
//  Copyright (c) 2014 brightec. All rights reserved.
//

import UIKit

class SOPCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    let dateCellIdentifier = "DateCellIdentifier"
    let contentCellIdentifier = "ContentCellIdentifier"
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var sidebarButton: UIBarButtonItem!
    
    @IBOutlet weak var orgLabel: UILabel!
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var itemCostLabel: UILabel!
    
    var reachability: Reachability?
    var loadingView : UIView?
    var yearlyData  = [String:AnyObject]()
    
    var headers:[String] = []
    var sopTitle:[String] = []
    var organization:String?
    var items:String?
    var itemCost:String?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Item Details - Yearly"
        
        guard let organization = yearlyData["organization"]!["name"]  else {
            return
        }

        guard let items = yearlyData["item"]!["name"]  else {
            return
        }
        
        guard let itemCost = yearlyData["itemCost"]!["name"]  else {
            return
        }
        
        self.organization = organization as? String
        self.items = items as? String
        
        
        orgLabel.text = "Organization: " + self.organization!
        itemLabel.text =  "Items: " + self.items!
        
        
        guard let supply = yearlyData["totalSupply"]!["name"]  else {
            return
        }
        
        guard let demand = yearlyData["totalDemand"]!["name"]  else {
            return
        }
        
        guard let cost = yearlyData["itemCost"]!["name"]  else {
            return
        }
        
        guard let excess = yearlyData["excess"]!["name"]  else {
            return
        }
        
        guard let projectedEndingBalance = yearlyData["projectedEndingBalance"]!["name"]  else {
            return
        }
        
        self.itemCost = String(cost as! Int)
        itemCostLabel.text = "Item Cost: " + self.itemCost!
        
        sopTitle = ["Year:", "Total Supply:", "Total Demand:", "Excess:", "Ending Balance(Proj):"]
        
        headers.append("Year:")
        headers.append(String(supply as! Int))
        headers.append(String(demand as! Int))
        //headers.append(String(cost as! Int))
        headers.append(String(excess as! Int))
        headers.append(String(projectedEndingBalance as! Int))
 
        
        self.collectionView .registerNib(UINib(nibName: "DateCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: dateCellIdentifier)
        self.collectionView .registerNib(UINib(nibName: "ContentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: contentCellIdentifier)
        

    }
    
    
    
    
    // MARK - UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 5
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let dateCell : DateCollectionViewCell = collectionView .dequeueReusableCellWithReuseIdentifier(dateCellIdentifier, forIndexPath: indexPath) as! DateCollectionViewCell
                dateCell.dateLabel.font = UIFont.boldSystemFontOfSize(15)
                dateCell.dateLabel.textColor = UIColor.darkGrayColor()
                dateCell.dateLabel.text = "Year: "
                
                return dateCell
            } else {
                let contentCell : ContentCollectionViewCell = collectionView .dequeueReusableCellWithReuseIdentifier(contentCellIdentifier, forIndexPath: indexPath) as! ContentCollectionViewCell
                contentCell.contentLabel.font = UIFont.boldSystemFontOfSize(15)
                contentCell.contentLabel.textColor = UIColor.darkGrayColor()
                contentCell.contentLabel.text = "2016 "
                return contentCell
            }
        } else {
            if indexPath.row == 0 {
                let dateCell : DateCollectionViewCell = collectionView .dequeueReusableCellWithReuseIdentifier(dateCellIdentifier, forIndexPath: indexPath) as! DateCollectionViewCell
                dateCell.dateLabel.font = UIFont.boldSystemFontOfSize(15)
                dateCell.dateLabel.textColor = UIColor.darkGrayColor()
                dateCell.dateLabel.text =  sopTitle[indexPath.section]
                
                return dateCell
            } else {
                let contentCell : ContentCollectionViewCell = collectionView .dequeueReusableCellWithReuseIdentifier(contentCellIdentifier, forIndexPath: indexPath) as! ContentCollectionViewCell
                contentCell.contentLabel.font = UIFont.systemFontOfSize(15)
                contentCell.contentLabel.textColor = UIColor.darkGrayColor()
                contentCell.contentLabel.text =  headers[indexPath.section]
                
                return contentCell
            }
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath){

        let backItem = UIBarButtonItem()
        backItem.title = "Yearly"
        navigationItem.backBarButtonItem = backItem

        let monthlyView = self.storyboard?.instantiateViewControllerWithIdentifier("SOPMonthly") as? SOPMonthCollectionViewController
        
        monthlyView!.monthlyData = yearlyData
        monthlyView!.organization = self.organization
        monthlyView!.items = self.items
        monthlyView!.headers = headers
        monthlyView!.sopTitle = sopTitle
        
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.pushViewController(monthlyView!, animated: true)

    }
}


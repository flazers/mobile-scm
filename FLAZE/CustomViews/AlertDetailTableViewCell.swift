//
//  AlertDetailTableViewCell.swift
//  FLAZE
//
//  Created by swamy kottu on 5/1/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import UIKit

class AlertDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var keyLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        keyLabel.textAlignment = NSTextAlignment.Left
        keyLabel.font = UIFont.boldSystemFontOfSize(15)
        keyLabel.textColor = UIColor.darkGrayColor()
        
        valueLabel.textAlignment = NSTextAlignment.Left
        valueLabel.font = UIFont.boldSystemFontOfSize(15)
        valueLabel.textColor = UIColor.darkGrayColor()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

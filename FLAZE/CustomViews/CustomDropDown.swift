//
//  CustomDropDown.swift
//  FLAZE
//
//  Created by swamy kottu on 3/27/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import UIKit

@IBDesignable
class CustomDropDown: UIButton {

    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
        
        //// Bezier Drawing
        let bezierPath = UIBezierPath()
        bezierPath.moveToPoint(CGPointMake(10, 10))
        bezierPath.addLineToPoint(CGPointMake(20, 10))
        bezierPath.addLineToPoint(CGPointMake(15, 20))
        bezierPath.addLineToPoint(CGPointMake(10.0, 10))
        bezierPath.addLineToPoint(CGPointMake(10.0, 10))
        bezierPath.closePath()
        UIColor.whiteColor().setFill()
        bezierPath.fill()
        UIColor.whiteColor().setStroke()
        bezierPath.lineWidth = 1
        bezierPath.stroke()
    }
    

}

//
//  Planner.swift
//  FLAZE
//
//  Created by Sista, Siva Rama Krishna on 5/19/16.
//  Copyright © 2016 com.swamy.kottu. All rights reserved.
//

import Foundation

class Planner: NSObject {
    var userId: String?
    var userName: String?
    
    override init() {
        super.init()
    }
    
    required init?(json: AnyObject) {
        super.init()
        
        guard let dict = json as? [String: AnyObject] else {
            return nil
        }
        
        if let id = dict["userId"] as? String {
            userId = id
        }
        
        if let name = dict["userName"] as? String {
            userName = name
        }
        
    }
}
